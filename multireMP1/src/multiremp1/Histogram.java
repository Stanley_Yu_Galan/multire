/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiremp1;

import java.awt.Color;

/**
 *
 * @author Stanley
 */
public class Histogram
    {
    public int red[] = new int[8];
    public int blue[] = new int[8];
    public int green[] = new int[8];    
    public float normRed[] = new float[8];
    public float normGreen[] = new float[8];
    public float normBlue[] = new float[8];

    void addToRed(int value)
      {
        red[value]++;
      }
    void addToGreen(int value)
      {
        green[value]++;
      }
    void addToBlue(int value)
      {
        blue[value]++;
      }

    int getTotal(String color)
      {int accumulator = 0;
         switch(color)
            {
            case "red": 
                for (int i=0; i<8;i++)
                {
                accumulator += red[i];
                };break;
            case "green": 
                for (int i=0; i<8;i++)
                {
                accumulator += green[i];
                };break;
            case "blue": 
                for (int i=0; i<8;i++)
                {
                accumulator += blue[i];
                };break;
            default: System.out.println("You messed up");return 9999;
            }
         return accumulator;
      }
            int get(String color, int value){
        switch(color)
            {
            case "red": return red[value];
            case "green": return green[value];
            case "blue": return blue[value];
            default: System.out.println("You messed up");return 9999;
            }

           }
            
            void set(String color, int index, float value){
                switch(color)
                    {
                    case "red":  normRed[index]=value;break;
                    case "green":  normGreen[index]=value;break;
                    case "blue":  normBlue[index]=value;break;
                    default: System.out.println("You messed up");
                    }

           }
     
     
     public void displayData()
       { System.out.print("Histogram: ");
  
         System.out.print("Red: ");
         for (int i=0; i<8;i++)
             {
             System.out.print(red[i]+" ");
             }
         System.out.print("\n");
         
         System.out.print("Green: ");
         for (int i=0; i<8;i++)
             {
             System.out.print(green[i]+" ");
             }
         System.out.print("\n");
         
         System.out.print("Blue: ");
         for (int i=0; i<8;i++)
             {
             System.out.print(blue[i]+" ");
             }
         System.out.print("\n");
       }
     
     public Color getMostAverageColor()
       {
         int redAverage =0;
         int greenAverage =0;
         int blueAverage =0;
         int itemCount =0;
         for(int i=0;i<8;i++)
             {
             //System.out.println("RedVal: " + red[i]);
             itemCount+=red[i];
//             System.out.println("red: "+((i*32)+16)*red[i]);
//             System.out.println("green: "+((i*32)+16)*green[i]);
//             System.out.println("blue: "+((i*32)+16)*blue[i]);
             redAverage += (((i*32)+16)*red[i]);
             greenAverage += (((i*32)+16)*green[i]);
             blueAverage += (((i*32)+16)*blue[i]);
             }
         redAverage /= itemCount;
         greenAverage /= itemCount;
         blueAverage /= itemCount;
//        System.out.println("Average RED = " + redAverage);
//        System.out.println("Average Green  = " + greenAverage);
//        System.out.println("Average Blue = " + blueAverage);
            
         
       Color averageColor = new Color(redAverage, greenAverage,blueAverage);
       return averageColor;
       }
      public void displayNormData()
       { System.out.print("Histogram: ");
  
         System.out.print("Red: ");
         for (int i=0; i<8;i++)
             {
             System.out.print(normRed[i]+" ");
             }
         System.out.print("\n");
         
         System.out.print("Green: ");
         for (int i=0; i<8;i++)
             {
             System.out.print(normGreen[i]+" ");
             }
         System.out.print("\n");
         
         System.out.print("Blue: ");
         for (int i=0; i<8;i++)
             {
             System.out.print(normBlue[i]+" ");
             }
         System.out.print("\n");
       }
    }
