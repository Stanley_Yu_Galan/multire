/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiremp1;

import java.awt.Color;

/**
 *
 * @author Olivia
 */
public class ColorCoherence {
    //static CandidateImage queryImage = new CandidateImage();

    public int connectedRed[] = new int[16];
    public int connectedBlue[] = new int[16];
    public int connectedGreen[] = new int[16];
    public int nonConnectedRed[] = new int[16];
    public int nonConnectedBlue[] = new int[16];
    public int nonConnectedGreen[] = new int[16];
    public int passedby [][];   
    
    public void clearData()
      {
        connectedRed = new int[16];
        connectedBlue = new int[16];
        connectedGreen = new int[16];
        nonConnectedRed = new int[16];
        nonConnectedBlue = new int[16];
        nonConnectedGreen  = new int[16];
      }
    public void setCCRed (Color [][] image) {
        clearData();
        passedby = new int [image.length - 1][image[0].length - 1];
        for (int i = 0; i < image.length - 1; i++) {
            for (int j = 0; j < image[0].length - 1; j ++) {
                if (passedby [i][j] != 1) {
                    int ccCount = cCRed (image, i, j, 0);
                    if (ccCount < 6) {
                        nonConnectedRed[image[i][j].getRed() / 16] += ccCount;
                    } else {
                        connectedRed[image[i][j].getRed()  / 16] += ccCount;
                    }
                }
            }
        }
        passedby = null;
    }
    
    public int cCRed (Color[][] image, int x, int y, int count) {
        count ++;
        passedby[x][y] = 1;
        if (x + 1 < image.length - 1 && 
                passedby [x + 1][y] != 1 && 
                image [x][y].getRed() / 16 == image [x + 1][y].getRed() / 16) {
            count = cCRed(image, x + 1, y, count);
        } 
        if (y + 1 < image [x].length - 1 && 
                passedby [x][y + 1] != 1 && 
                image [x][y].getRed() / 16 == image [x][y + 1].getRed() / 16) {
            count = cCRed(image, x, y + 1, count);
        }
        if (x - 1 > -1 && 
                passedby [x - 1][y] != 1 && 
                image [x][y].getRed() / 16 == image [x - 1][y].getRed() / 16) {
            count = cCRed(image, x - 1, y, count);
        } 
        if (y - 1 > -1 && 
                passedby [x][y - 1] != 1 && 
                image [x][y].getRed() / 16 == image [x][y - 1].getRed() / 16) {
            count = cCRed(image, x, y - 1, count);
        }
        return count;
    }
    
    public void setCCBlue (Color [][] image) {
        clearData();
        passedby = new int [image.length - 1][image[0].length - 1];
        for (int i = 0; i < image.length - 1; i++) {
            for (int j = 0; j < image[0].length - 1; j ++) {
                if (passedby [i][j] != 1) {
                    int ccCount = cCBlue (image, i, j, 0);
                    if (ccCount < 6) {
                        nonConnectedBlue[image[i][j].getBlue() / 16] += ccCount;
                    } else {
                        connectedBlue[image[i][j].getBlue()  / 16] += ccCount;
                    }
                }
            }
        }
        passedby = null;
    }
    
    public int cCBlue (Color[][] image, int x, int y, int count) {
        count ++;
        passedby[x][y] = 1;
        if (x + 1 < image.length - 1 && 
                passedby [x + 1][y] != 1 && 
                image [x][y].getBlue() / 16 == image [x + 1][y].getBlue() / 16) {
            count = cCBlue(image, x + 1, y, count);
        } 
        if (y + 1 < image [x].length - 1 && 
                passedby [x][y + 1] != 1 && 
                image [x][y].getBlue() / 16 == image [x][y + 1].getBlue() / 16) {
            count = cCBlue(image, x, y + 1, count);
        }
        if (x - 1 > -1 && 
                passedby [x - 1][y] != 1 && 
                image [x][y].getBlue() / 16 == image [x - 1][y].getBlue() / 16) {
            count = cCBlue(image, x - 1, y, count);
        } 
        if (y - 1 > -1 && 
                passedby [x][y - 1] != 1 && 
                image [x][y].getBlue() / 16 == image [x][y - 1].getBlue() / 16) {
            count = cCBlue(image, x, y - 1, count);
        }
        return count;
    }
    
    public void setCCGreen (Color [][] image) {
        clearData();
        passedby = new int [image.length - 1][image[0].length - 1];
        for (int i = 0; i < image.length - 1; i++) {
            for (int j = 0; j < image[0].length - 1; j ++) {
                if (passedby [i][j] != 1) {
                    int ccCount = cCGreen (image, i, j, 0);
                    if (ccCount < 6) {
                        nonConnectedGreen[image[i][j].getGreen() / 16] += ccCount;
                    } else {
                        connectedGreen[image[i][j].getGreen()  / 16] += ccCount;
                    }
                }
            }
        }
        passedby = null;
    }
    
    public int cCGreen (Color[][] image, int x, int y, int count) {
        count ++;
        passedby[x][y] = 1;
        if (x + 1 < image.length - 1 && 
                passedby [x + 1][y] != 1 && 
                image [x][y].getGreen() / 16 == image [x + 1][y].getGreen() / 16) {
            count = cCGreen(image, x + 1, y, count);
        } 
        if (y + 1 < image [x].length - 1 && 
                passedby [x][y + 1] != 1 && 
                image [x][y].getGreen() / 16 == image [x][y + 1].getGreen() / 16) {
            count = cCGreen(image, x, y + 1, count);
        }
        if (x - 1 > -1 && 
                passedby [x - 1][y] != 1 && 
                image [x][y].getGreen() / 16 == image [x - 1][y].getGreen() / 16) {
            count = cCGreen(image, x - 1, y, count);
        } 
        if (y - 1 > -1 && 
                passedby [x][y - 1] != 1 && 
                image [x][y].getGreen() / 16 == image [x][y - 1].getGreen() / 16) {
            count = cCGreen(image, x, y - 1, count);
        }
        return count;
    }
    
   

    public int[] getConnectedRed() {
        return connectedRed;
    }

    public int[] getConnectedBlue() {
        return connectedBlue;
    }

    public int[] getConnectedGreen() {
        return connectedGreen;
    }

    public int[] getNonConnectedRed() {
        return nonConnectedRed;
    }

    public int[] getNonConnectedBlue() {
        return nonConnectedBlue;
    }

    public int[] getNonConnectedGreen() {
        return nonConnectedGreen;
    }
}
