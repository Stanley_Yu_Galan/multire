/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiremp1;

/**
 *
 * @author Aakov
 */
public class CIE_LUV {
    
    public double l;
    public double u;
    public double v;
    
    public CIE_LUV(){
        
    }
    
    public CIE_LUV(double l, double u, double v){
        this.l = l;
        this.u = u;
        this.v = v;
    }

    public double getL() {
        return l;
    }

    public void setL(double l) {
        this.l = l;
    }

    public double getU() {
        return u;
    }

    public void setU(double u) {
        this.u = u;
    }

    public double getV() {
        return v;
    }

    public void setV(double v) {
        this.v = v;
    }
    
}
