/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiremp1;

/**
 *
 * @author Aakov
 */
public class LUVHistogram {
    
    public int luvHist[] = new int[159];
    
    public int[] getLuvHist() {
        return luvHist;
    }

    public void setLuvHist(int[] luvHist) {
        this.luvHist = luvHist;
    }
    
    public void addHist(int index){
        luvHist[index]++;
    }
    
    public int getHistValue(int index){
        return luvHist[index];
    }
    
    public double getNormalizedHistValue(int index){
        return luvHist[index] / 159.0;
    }
    
    /*
    public int l[] = new int[6];
    public int u[] = new int[6];
    public int v[] = new int[6];
    public float normL[] = new float[6];
    public float normU[] = new float[6];
    public float normV[] = new float[6];
    
    void addToL(int value){
        l[value]++;
    }
    
    void addToU(int value){
        u[value]++;
    }
    
    void addToV(int value){
        v[value]++;
    }
    
    int getTotal(String color){
        int accumulator = 0;
        
        switch(color){
            case "L":
                for (int i = 0; i < 6; i++){
                    accumulator += l[i];
                }
            break;
            
            case "U":
                for (int i = 0; i < 6; i++){
                    accumulator += u[i];
                }
            break;
            
            case "V":
                for (int i = 0; i < 6; i++){
                    accumulator += v[i];
                }
            break;
            
            default:
                System.out.println("You messed up."); return 9999;
        }
        return accumulator;
    }
    
    int get(String color, int value){
        switch(color){
            case "L": return l[value];
            case "U": return u[value];
            case "V": return v[value];
            default: System.out.println("You messed up."); return 999;
        }
    }
    
    void set(String color, int index, float value){
        switch(color){
            case "L": normL[index] = value; break;
            case "U": normU[index] = value; break;
            case "V": normV[index] = value; break;
            default: System.out.println("You messed up.");
        }
    }
    */

}
