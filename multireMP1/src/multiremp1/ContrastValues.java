/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiremp1;

/**
 *
 * @author Stanley
 */
public class ContrastValues
    {
     public float[][] verticalContrastRed = new float[9][9];
     public float[][] verticalContrastGreen = new float[9][9];
     public float[][] verticalContrastBlue = new float[9][9];
     
     public float[][] horizontalContrastRed = new float[9][9];
     public float[][] horizontalContrastGreen = new float[9][9];
     public float[][] horizontalContrastBlue = new float[9][9];
     
     public float[][] contrastDeltaRed = new float[9][9];
     public float[][] contrastDeltaGreen = new float[9][9];
     public float[][] contrastDeltaBlue = new float[9][9];
    }
