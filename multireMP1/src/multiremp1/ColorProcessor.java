/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiremp1;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import javax.imageio.ImageIO;

/**
 *
 * @author Stanley
 */
public class ColorProcessor
    {
    public static int[][] convertTo2DWithoutUsingGetRGB(BufferedImage image) {

      final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
      final int width = image.getWidth();
      final int height = image.getHeight();
      final boolean hasAlphaChannel = image.getAlphaRaster() != null;

      int[][] result = new int[height][width];
      if (hasAlphaChannel) {
         final int pixelLength = 4;
         for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
            int argb = 0;
            argb += (((int) pixels[pixel] & 0xff) << 24); // alpha
            argb += ((int) pixels[pixel + 1] & 0xff); // blue
            argb += (((int) pixels[pixel + 2] & 0xff) << 8); // green
            argb += (((int) pixels[pixel + 3] & 0xff) << 16); // red
            result[row][col] = argb;
            col++;
            if (col == width) {
               col = 0;
               row++;
            }
         }
      } else {
         final int pixelLength = 3;
         for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
            int argb = 0;
            argb += -16777216; // 255 alpha
            argb += ((int) pixels[pixel] & 0xff); // blue
            argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
            argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
            result[row][col] = argb;
            col++;
            if (col == width) {
               col = 0;
               row++;
            }
         }
      }

      return result;
   }
    
    
      public static Color[][] bufferedImageToColorArray(BufferedImage image) {

      final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
      final int width = image.getWidth();
      final int height = image.getHeight();
      final boolean hasAlphaChannel = image.getAlphaRaster() != null;

      int[][] result = new int[height][width];
      if (hasAlphaChannel) {
         final int pixelLength = 4;
         for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
            int argb = 0;
            argb += (((int) pixels[pixel] & 0xff) << 24); // alpha
            argb += ((int) pixels[pixel + 1] & 0xff); // blue
            argb += (((int) pixels[pixel + 2] & 0xff) << 8); // green
            argb += (((int) pixels[pixel + 3] & 0xff) << 16); // red
            result[row][col] = argb;
            col++;
            if (col == width) {
               col = 0;
               row++;
            }
         }
      } else {
         final int pixelLength = 3;
         for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
            int argb = 0;
            argb += -16777216; // 255 alpha
            argb += ((int) pixels[pixel] & 0xff); // blue
            argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
            argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
            result[row][col] = argb;
            col++;
            if (col == width) {
               col = 0;
               row++;
            }
         }
      }
   Color[][] colorArray = new Color[image.getHeight()-1][image.getWidth()-1];            
            for(int i =0;i< image.getHeight()-1; i++)
                {
                    for(int j =0;j< image.getWidth()-1; j++)
                    {
                    colorArray[i][j] = new Color(result[i][j]);
                    }
                }
            //Histogram histocram = createHistogram(colorArray);
       return colorArray;
   }
      public static void displayRGBValues(Color[][] input)
        {System.out.println("HistoPrint: ");
          for(int i =0;i< input.length-1; i++)
                {
                    for(int j =0;j< input[0].length-1; j++)
                    {
                    System.out.print(input[i][j]);
                    }
                    System.out.println("/n");
                }
        }
      
      public static Histogram createHistogram(Color[][] input)
        {      
          Histogram histogram = new Histogram();
          //System.out.println("Choppy: " +(input.length-1) + " x "+ (input[0].length-1));
          for(int i =0;i< input.length-1; i++)
                {
                    for(int j =0;j< input[0].length-1; j++)
                        {
                        histogram.addToRed(input[i][j].getRed()/32);
                        histogram.addToGreen(input[i][j].getGreen()/32);
                        histogram.addToBlue(input[i][j].getBlue()/32);
                        }
                    //System.out.println("/n");
                }
        
          float hyperTotal =histogram.getTotal("red") +histogram.getTotal("green")+histogram.getTotal("blue");
                    for(int j =0;j<8; j++)
                        {
                        histogram.set("red",j,(float)histogram.get("red",j)/hyperTotal);
                        histogram.set("green",j,(float)histogram.get("green",j)/hyperTotal);
                        histogram.set("blue",j,(float)histogram.get("blue",j)/hyperTotal);
                        }
               
          
         return histogram;
        }
      
      public static float[][] nineByNineContrastDelta(Color[][] input, String color)
        {      
          int blockDimI= (input.length-1)/8;
          int blockDimJ= (input[0].length-1)/8;
          int iOffset = 0;
          int jOffset = 0;
          int counter =0;
          float[][] contrastDelta =new float[9][9];
          for(int i=0;i<=8;i++)
              {
                for(int j=0;j<=8;j++)
                {
                counter++;
                iOffset= blockDimI*i;
                jOffset= blockDimJ*j;
                //System.out.println("I value: " + i+" J value: "+ j );
                //System.out.println("I offset: " + iOffset+" J offset: "+ jOffset );
                for(int a = iOffset; a< iOffset+blockDimI && a<input.length-1;a++)
                    {
                    for(int b = jOffset; b< jOffset+blockDimJ && b < input[0].length-1;b++)
                        {
                        //System.out.println("X: "+a +" Y: "+b);
                        float tempVar=0 ;
                        if(b-1>0 && a+1<input.length && b+1<input[0].length && a>0){
                            switch(color){
                        case "red":
                        tempVar+= Math.abs(input[a][b].getRed()-input[a-1][b-1].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a-1][b].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a-1][b+1].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a][b-1].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a][b+1].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a+1][b-1].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a+1][b].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a+1][b+1].getRed());break;
                        
                        case "green":
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a-1][b-1].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a-1][b].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a-1][b+1].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a][b-1].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a][b+1].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a+1][b-1].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a+1][b].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a+1][b+1].getGreen());break;
                        
                        case "blue" :
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a-1][b-1].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a-1][b].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a-1][b+1].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a][b-1].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a][b+1].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a+1][b-1].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a+1][b].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a+1][b+1].getBlue());break;
                        
                        default:System.out.println("SWITCH FAILE!!");
                            }
                        contrastDelta[i][j] += tempVar/9;
//                        System.out.println(i+":"+j+ " val= "+ tempVar/9);
//                        System.out.println(contrastDelta[i][j]);
                        }
                        
                        }
                    }
                                
                }  
              }
                  return contrastDelta;
        }
      
      
      public static float[][] nineByNineVerticalContrast(Color[][] input, String color)
        {      
          int blockDimI= (input.length-1)/8;
          int blockDimJ= (input[0].length-1)/8;
          int iOffset = 0;
          int jOffset = 0;
          int counter =0;
          float[][] verticalContrast =new float[9][9];
          for(int i=0;i<=8;i++)
              {
                for(int j=0;j<=8;j++)
                {
                counter++;
                iOffset= blockDimI*i;
                jOffset= blockDimJ*j;
                //System.out.println("I value: " + i+" J value: "+ j );
                //System.out.println("I offset: " + iOffset+" J offset: "+ jOffset );
                for(int a = iOffset; a< iOffset+blockDimI && a<input.length-1;a++)
                    {
                    for(int b = jOffset; b< jOffset+blockDimJ && b < input[0].length-1;b++)
                        {
                        //System.out.println("X: "+a +" Y: "+b);
                        float tempVar=0 ;
                        if(b-1>0 && a+4<input.length && b+1<input[0].length && a>0){
                            switch(color){
                        case "red":
                        tempVar+= Math.abs(input[a][b].getRed()-input[a+1][b].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a+2][b].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a+3][b].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a+4][b].getRed());break;
                        
                        case "green":
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a+1][b].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a+2][b].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a+3][b].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a+4][b].getGreen());break;
                        
                        case "blue":
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a+1][b].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a+2][b].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a+3][b].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a+4][b].getBlue());break;
                        default:System.out.println("SWITCH FAILE!!");
                            }
                        verticalContrast[i][j] += tempVar/4;
//                        System.out.println(i+":"+j+ " val= "+ tempVar/9);
//                        System.out.println(contrastDelta[i][j]);
                        }
                        
                        }
                    }
                                
                }  
              }
                  return verticalContrast;
        }
      
      
      public static float[][] nineByNineHorizontalContrast(Color[][] input, String color)
        {      
          int blockDimI= (input.length-1)/8;
          int blockDimJ= (input[0].length-1)/8;
          int iOffset = 0;
          int jOffset = 0;
          int counter =0;
          float[][] horizontalContrast =new float[9][9];
          for(int i=0;i<=8;i++)
              {
                for(int j=0;j<=8;j++)
                {
                counter++;
                iOffset= blockDimI*i;
                jOffset= blockDimJ*j;
                //System.out.println("I value: " + i+" J value: "+ j );
                //System.out.println("I offset: " + iOffset+" J offset: "+ jOffset );
                for(int a = iOffset; a< iOffset+blockDimI && a<input.length-1;a++)
                    {
                    for(int b = jOffset; b< jOffset+blockDimJ && b < input[0].length-1;b++)
                        {
                        //System.out.println("X: "+a +" Y: "+b);
                        float tempVar=0 ;
                        if(b-1>0 && a+1<input.length && b+4<input[0].length && a>0){
                            
                            switch(color){
                        case "red":
                        tempVar+= Math.abs(input[a][b].getRed()-input[a][b+1].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a][b+2].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a][b+3].getRed());
                        tempVar+= Math.abs(input[a][b].getRed()-input[a][b+4].getRed());break;
                        
                        case "green":
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a][b+1].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a][b+2].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a][b+3].getGreen());
                        tempVar+= Math.abs(input[a][b].getGreen()-input[a][b+4].getGreen());break;
                        
                        case "blue":
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a][b+1].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a][b+2].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a][b+3].getBlue());
                        tempVar+= Math.abs(input[a][b].getBlue()-input[a][b+4].getBlue());break;
                        default:System.out.println("SWITCH FAILE!!");
                        }
                        horizontalContrast[i][j] += tempVar/4;
                            
                            
//                        System.out.println(i+":"+j+ " val= "+ tempVar/9);
//                        System.out.println(contrastDelta[i][j]);
                        }
                        
                        }
                    }
                                
                }  
              }
                  return horizontalContrast;
        }
      
      public static float compareContrastDelta(CandidateImage query, CandidateImage candidate)
        {
          Float accumulator =0f;
          for(int x=0; x<9;x++){
              for(int y=0; y<9;y++)
                  {
                        accumulator +=(Math.abs(query.getContrastDeltaRed()[x][y]-candidate.getContrastDeltaRed()[x][y]) );
                        accumulator +=(Math.abs(query.getContrastDeltaGreen()[x][y]-candidate.getContrastDeltaGreen()[x][y]) );
                        accumulator +=(Math.abs(query.getContrastDeltaBlue()[x][y]-candidate.getContrastDeltaBlue()[x][y]) );
            }   
          }
              return accumulator/3;
        }
      
       public static float compareVerticalContrast(CandidateImage query, CandidateImage candidate)
        {
          Float accumulator =0f;
          for(int x=0; x<9;x++){
              for(int y=0; y<9;y++)
                  {
                        accumulator +=(Math.abs(query.getVerticalContrastRed()[x][y]-candidate.getVerticalContrastRed()[x][y]) );
                        accumulator +=(Math.abs(query.getVerticalContrastGreen()[x][y]-candidate.getVerticalContrastGreen()[x][y]) );
                        accumulator +=(Math.abs(query.getVerticalContrastBlue()[x][y]-candidate.getVerticalContrastBlue()[x][y]) );
         
                  
                  }   
          }
              return accumulator/3;
        }
       
       public static float compareHorizontalContrast(CandidateImage query, CandidateImage candidate)
        {
          Float accumulator =0f;
          for(int x=0; x<9;x++){
              for(int y=0; y<9;y++)
                  {
                        accumulator +=(Math.abs(query.getHorizontalContrastRed()[x][y]-candidate.getHorizontalContrastRed()[x][y]) );
                        accumulator +=(Math.abs(query.getHorizontalContrastGreen()[x][y]-candidate.getHorizontalContrastGreen()[x][y]) );
                        accumulator +=(Math.abs(query.getHorizontalContrastBlue()[x][y]-candidate.getHorizontalContrastBlue()[x][y]) );
            }   
          }
              return accumulator/3;
        }
      public static Histogram[][] nineByNineChunking(Color[][] input)
        {      
          int blockDimI= (input.length-1)/8;
          int blockDimJ= (input[0].length-1)/8;
          int iOffset = 0;
          int jOffset = 0;
          int counter =0;
          Histogram[][] histogram =new Histogram[9][9];
          for(int i=0;i<=8;i++)
              {
                for(int j=0;j<=8;j++)
                {
                histogram[i][j]= new Histogram();
                counter++;
                iOffset= blockDimI*i;
                jOffset= blockDimJ*j;
                //System.out.println("I value: " + i+" J value: "+ j );
                //System.out.println("I offset: " + iOffset+" J offset: "+ jOffset );
                for(int a = iOffset; a< iOffset+blockDimI && a<input.length-1;a++)
                    {
                    for(int b = jOffset; b< jOffset+blockDimJ && b < input[0].length-1;b++)
                        {
                        //System.out.println("X: "+a +" Y: "+b);
                        histogram[i][j].addToRed(input[a][b].getRed()/32);
                        histogram[i][j].addToGreen(input[a][b].getGreen()/32);
                        histogram[i][j].addToBlue(input[a][b].getBlue()/32);
                        }
                    }
                                
                }  
              }
//              for(int i=0;i<=8;i++)
//              {
//                for(int j=0;j<=8;j++)
//                {               
//                System.out.println(histogram[i][j].getMostAverageColor());
//                }
//              }     
                  //System.out.println("ENDED");
                  
                  return histogram;
        }
      
      
      public static Float chunkedHistogramComparison(CandidateImage query, CandidateImage candidate)
        {
          Float accumulator =0f;
          for(int x=0; x<9;x++){
              for(int y=0; y<9;y++)
                  {
                  //System.out.println("SORT OF "+ candidate.getChunkedHistogram()[0][0]);
          for(int i =0;i< 8; i++)
                        {
                        accumulator +=(Math.abs(query.getChunkedHistogram()[x][y].get("red", i)-candidate.getChunkedHistogram()[x][y].get("red", i)) );
                        accumulator +=(Math.abs(query.getChunkedHistogram()[x][y].get("green", i)-candidate.getChunkedHistogram()[x][y].get("green", i)) );
                        accumulator +=(Math.abs(query.getChunkedHistogram()[x][y].get("blue", i)-candidate.getChunkedHistogram()[x][y].get("blue", i)) );
                        
                        }
      
            }   
          }
              return accumulator/81;
        }
     public static Histogram createCenterCropHistogram(Color[][] input)
        {      
          //System.out.println("Getting crop value");
          float CROP_FACTOR =1.41f;
          Histogram histogram = new Histogram();
          //System.out.println("Choppy: ");
          int lengthPadding = (int)(input.length-(input.length/CROP_FACTOR))/2;
          int heightPadding = (int)(input[0].length-(input[0].length/CROP_FACTOR))/2; 
          for(int i =lengthPadding;i< input.length-lengthPadding; i++)
                { 
                    for(int j =heightPadding;j< input[0].length-heightPadding; j++)
                        {
                       // System.out.println("I: "+i +"J: "+j);
                        histogram.addToRed(input[i][j].getRed()/32);
                        histogram.addToGreen(input[i][j].getGreen()/32);
                        histogram.addToBlue(input[i][j].getBlue()/32);
                        }
                }
        
          float hyperTotal =histogram.getTotal("red") +histogram.getTotal("green")+histogram.getTotal("blue");
                    for(int j =0;j<8; j++)
                        {
                        histogram.set("red",j,(float)histogram.get("red",j)/hyperTotal);
                        histogram.set("green",j,(float)histogram.get("green",j)/hyperTotal);
                        histogram.set("blue",j,(float)histogram.get("blue",j)/hyperTotal);
                        }
         return histogram;
        }
      public static Histogram createOuterHistogram(Color[][] input)
        {      
          //System.out.println("Getting center value");
          float CROP_FACTOR =1.41f;
          Histogram histogram = new Histogram();
          //System.out.println("Choppy: ");
          int lengthPadding = (int)(input.length-(input.length/CROP_FACTOR))/2;
          int heightPadding = (int)(input[0].length-(input[0].length/CROP_FACTOR))/2; 
          
          for(int i =0;i< input.length-1; i++)
                { 
                    for(int j =0;j< input[0].length-1; j++)
                        {if((i<lengthPadding || i>input.length / CROP_FACTOR+lengthPadding)&&(j<heightPadding || j>input[0].length / CROP_FACTOR+heightPadding))
                            {histogram.addToRed(input[i][j].getRed()/32);
                             histogram.addToGreen(input[i][j].getGreen()/32);
                             histogram.addToBlue(input[i][j].getBlue()/32);
                            }
                        }
                    //System.out.println("/n");
                }
        
          float hyperTotal =histogram.getTotal("red") +histogram.getTotal("green")+histogram.getTotal("blue");
                    for(int j =0;j<8; j++)
                        {
                        histogram.set("red",j,(float)histogram.get("red",j)/hyperTotal);
                        histogram.set("green",j,(float)histogram.get("green",j)/hyperTotal);
                        histogram.set("blue",j,(float)histogram.get("blue",j)/hyperTotal);
                        }
                    
         return histogram;
        }
        public static Float  colorHistogramComparison(CandidateImage query, CandidateImage candidate)
        {
          Float accumulator =0f;
          for(int i =0;i< 8; i++)
                        {
                        accumulator +=(Math.abs(query.getHistogram().get("red", i)-candidate.getHistogram().get("red", i)) );
                        accumulator +=(Math.abs(query.getHistogram().get("green", i)-candidate.getHistogram().get("green", i)) );
                        accumulator +=(Math.abs(query.getHistogram().get("blue", i)-candidate.getHistogram().get("blue", i)) );
                        
                        }
          return accumulator;
        }   
        
        public static float  colorCoherenceComparison(CandidateImage query, CandidateImage candidate)
        {
          Float accumulator =0f;
          for(int i =0;i< 16; i++)
                        {
                        accumulator +=(Math.abs(query.colorCoherence.getConnectedRed()[i]-candidate.colorCoherence.getConnectedRed()[i]) );
                        accumulator +=(Math.abs(query.colorCoherence.getConnectedGreen()[i]-candidate.colorCoherence.getConnectedGreen()[i]) );
                        accumulator +=(Math.abs(query.colorCoherence.getConnectedBlue()[i]-candidate.colorCoherence.getConnectedBlue()[i]) );
                        
                        accumulator +=(Math.abs(query.colorCoherence.getNonConnectedRed()[i]-candidate.colorCoherence.getNonConnectedRed()[i]) );
                        accumulator +=(Math.abs(query.colorCoherence.getNonConnectedGreen()[i]-candidate.colorCoherence.getNonConnectedGreen()[i]) );
                        accumulator +=(Math.abs(query.colorCoherence.getNonConnectedBlue()[i]-candidate.colorCoherence.getNonConnectedBlue()[i]) );
                        
                        }
         // System.out.println("THE ACUM SAYS: "+ accumulator/6);
          return accumulator/6;
        }   
        
        public static Float  centerCropColorHistogramComparison(CandidateImage query, CandidateImage candidate)
        {
          Float accumulator =0f;
          for(int i =0;i< 8; i++)
                        {
                        accumulator +=(Math.abs(query.getCenterCropHistogram().get("red", i)-candidate.getCenterCropHistogram().get("red", i)) );
                        accumulator +=(Math.abs(query.getCenterCropHistogram().get("green", i)-candidate.getCenterCropHistogram().get("green", i)) );
                        accumulator +=(Math.abs(query.getCenterCropHistogram().get("blue", i)-candidate.getCenterCropHistogram().get("blue", i)) );
                         
                        accumulator +=(Math.abs(query.getOuterHistogram().get("red", i)-candidate.getOuterHistogram().get("red", i)) );
                        accumulator +=(Math.abs(query.getOuterHistogram().get("green", i)-candidate.getOuterHistogram().get("green", i)) );
                        accumulator +=(Math.abs(query.getOuterHistogram().get("blue", i)-candidate.getOuterHistogram().get("blue", i)) );
                       
                        }
          accumulator/=2;
          return accumulator;
        }   
      
       // LUV //
        
        public static Double simPerColLUV(CandidateImage query, CandidateImage candidate, double[][] simMatrix){
            Double accumulator = 0.000;
            
            for(int i = 0; i < 159; i++){
                for (int j = 0; j < 159; j++){
                    
                    if (Math.max(query.getLUVHistogram().getNormalizedHistValue(i), candidate.getLUVHistogram().getNormalizedHistValue(j)) != 0.000){
                        accumulator += (1 - (Math.abs(query.getLUVHistogram().getNormalizedHistValue(i) - candidate.getLUVHistogram().getNormalizedHistValue(j)) /
                                         Math.max(query.getLUVHistogram().getNormalizedHistValue(i), candidate.getLUVHistogram().getNormalizedHistValue(j)))) *
                                     simMatrix[i][j];
                    }
                }
            }
            
            return accumulator;
        }
        
        public static Double simColLUV(CandidateImage query, CandidateImage candidate, double[][] simMatrix){
            Double accumulator = 0.000;
            Double simExactCol = 0.000;
            Double simColor = 0.000;
            Double simPerCol = simPerColLUV(query, candidate, simMatrix);
            
            for(int i = 0; i < 159; i++){
                // get the colors within the threshold
                if(query.getLUVHistogram().getNormalizedHistValue(i) > 0.005){
                    simExactCol = (1 - (Math.abs(query.getLUVHistogram().getNormalizedHistValue(i) - candidate.getLUVHistogram().getNormalizedHistValue(i)) /
                                        Math.max(query.getLUVHistogram().getNormalizedHistValue(i), candidate.getLUVHistogram().getNormalizedHistValue(i))));
                    
                    simColor = simExactCol * (1 + simPerCol);
                    
                    accumulator += simColor * query.getLUVHistogram().getNormalizedHistValue(i);
                }
            }
            
            return accumulator;
        }
        
        // Converts RGB values to LUV and adds it to the histogram
       public static LUVHistogram convertToLUV(Color[][] input)
        {
            cieConvert conv = new cieConvert();
            LUVHistogram luvHist = new LUVHistogram();
            
            CIE_LUV[][] luvArray = new CIE_LUV[input.length][input[0].length];
            
            for(int i = 0; i < input.length-1; i++){
                for(int j = 0; j < input[i].length-1; j++){
                    conv.setValues(input[i][j].getRed() / 255.000, input[i][j].getBlue() / 255.000, input[i][j].getGreen() / 255.000);
                    
                    CIE_LUV luv = new CIE_LUV();
                    luv.setL(conv.L);
                    luv.setU(conv.u);
                    luv.setV(conv.v);
                    
                    luvArray[i][j] = luv;
                    
//                    System.out.println("L: " + luv.getL());
//                    System.out.println("U: " + luv.getU());
//                    System.out.println("V: " + luv.getV());
                    
                    luvHist.addHist(conv.IndexOf());
                }
            }
            
            return luvHist;
            
            /*
            CIE_LUV[][] luvArray = new CIE_LUV[input.length][input[0].length];
            
            for(int i = 0; i < input.length-1; i++){
                for(int j = 0; j < input[i].length-1; j++){
                    
//                    System.out.println("Red: " + input[i][j].getRed());
//                    System.out.println("Green: " + input[i][j].getGreen());
//                    System.out.println("Blue: " + input[i][j].getBlue());
                    
                    // Convert RGB to XYZ
                    double r = input[i][j].getRed() / 255.000;
                    double g = input[i][j].getGreen() / 255.000;
                    double b = input[i][j].getBlue() / 255.000;
                    
//                    System.out.println("Red: " + r);
//                    System.out.println("Green: " + g);
//                    System.out.println("Blue: " + b);
                    
                    if (r > 0.04045)        r = Math.pow(((r + 0.055) / 1.055), 2.400);
                    else                    r = r / 12.920;
                    
                    if (g > 0.04045)        g = Math.pow(((g + 0.055) / 1.055), 2.400);
                    else                    g = g / 12.920;
                    
                    if (b > 0.04045)        b = Math.pow(((b + 0.055) / 1.055), 2.400);
                    else                    g = g / 12.920;
                    
                    r = r * 100.000;
                    g = g * 100.000;
                    b = b * 100.000;
                    
                    double x = (r * 0.412453) + (g * 0.357580) + (b * 0.180423);
                    double y = (r * 0.212671) + (g * 0.715160) + (b * 0.072169);
                    double z = (r * 0.019334) + (g * 0.119193) + (b * 0.950227);
                    
//                    System.out.println("X: " + x);
//                    System.out.println("Y: " + y);
//                    System.out.println("Z: " + z);
                    
                    // XYZ to LUV
                    double u = (4.000 * x) / (x + (15.000 * y) + (3.000 * z));
                    double v = (9.000 * y) / (x + (15.000 * y) + (3.000 * z));
                    
                    double var_y = y / 100.000;
                    if (var_y > 0.008856)       var_y = Math.pow(var_y, (1.000/3.000));
                    else                        var_y = (7.787 * var_y) + (16.000 / 116.000);
                    
                    double ref_x = 95.047;
                    double ref_y = 100.000;
                    double ref_z = 108.883;
                    
                    double ref_u = (4.000 * ref_x) / (ref_x + (15.000 * ref_y) + (3.000 * ref_z));
                    double ref_v = (9.000 * ref_y) / (ref_x + (15.000 * ref_y) + (3.000 * ref_z));
                    
                    CIE_LUV luv = new CIE_LUV();
                    luv.setL((116.000 * var_y) - 16.000);
                    luv.setU(13.000 * luv.getL() * (u - ref_u));
                    luv.setV(13.000 * luv.getL() * (v - ref_v));
                    
//                    System.out.println("L: " + (int) luv.getL());
//                    System.out.println("U: " + (int) luv.getU());
//                    System.out.println("V: " + (int) luv.getV());
                    
//                    System.out.println("L: " + (int) (luv.getL() / 20));
//                    System.out.println("U: " + (int) ((luv.getU()+134) / 70.8));
//                    System.out.println("V: " + (int) ((luv.getV()+140) / 52.4));
                    
                    luvArray[i][j] = luv;
                }
            }
     s       
          return luvArray;
                    */
        }
      
       
       /*
       public static Float getMaxDifference(CandidateImage query, CandidateImage candidate){
           Float max = -999f;
           Float accumulator = 0f;
           
           for (int i = 0; i < 6; i++){
              accumulator = 0f;
              
              accumulator += (Math.abs(query.getLUVHistogram().get("L", i) - candidate.getLUVHistogram().get("L", i)));
              accumulator += (Math.abs(query.getLUVHistogram().get("U", i) - candidate.getLUVHistogram().get("U", i)));
              accumulator += (Math.abs(query.getLUVHistogram().get("V", i) - candidate.getLUVHistogram().get("V", i)));
              
              if (accumulator > max){
                  max = accumulator;
              }
           }
           
           return max;
       }
    
   */
    }
