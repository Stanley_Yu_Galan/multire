/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiremp1;

import java.awt.Color;
import java.awt.Image;
import java.awt.color.ColorSpace;
import static java.awt.color.ColorSpace.TYPE_Luv;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 *
 * @author Stanley
 */
public class GUI extends javax.swing.JFrame
    {

    CandidateImage queryImage = new CandidateImage();
    ArrayList<CandidateImage> candidates = new ArrayList();
    String IMAGE_DIRECTORY = "C:/Users/Stanley/Documents/multire/Samples/images";
    
    cieConvert conv = new cieConvert();
    double[][] simMatrix;
    
    /**
     * Creates new form GUI
     */
    public GUI()
      {
          // Precompute matrix first
          conv.initLuvIndex();
          
          double diff;
          double max = -999;
          double t;
          
          // Initialize NxN matrix
          simMatrix = new double[159][159];
          for (int i = 0; i < 159; i++){
              for (int j = 0; j < 159; j++){
                  // Get the difference between each color level using the Euchlidean distance
                  diff = Math.sqrt(Math.pow(conv.getValue("L", i) - conv.getValue("L", j), 2) + 
                                   Math.pow(conv.getValue("U", i) - conv.getValue("U", j), 2) +
                                   Math.pow(conv.getValue("V", i) - conv.getValue("V", j), 2));
                  
                  // Check if this is the maximum value and get the Tcolor
                  if (diff > max){
                      max = diff;
                  }
                  
                  t = 0.2 * max;
                  
                  if (diff > t){
                      simMatrix[i][j] = 0;
                  } else if (t == 0){
                      simMatrix[i][j] = 1 - (diff / 1);
                  } else {
                      simMatrix[i][j] = 1 - (diff / t);
                  }
                  
                  //DecimalFormat df = new DecimalFormat("0.0");
                  //System.out.print(df.format(simMatrix[i][j]) + " | ");
                  
              }
              //System.out.println();
          }
          
          
        initComponents();
      }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        resultsPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setText("Load Image");
        jButton1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Query Image");

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/multiremp1/wood_floor_by_gnrbishop.jpg"))); // NOI18N
        jLabel2.setText("jLabel2");
        jLabel2.setAutoscrolls(true);

        jButton2.setText("Search");
        jButton2.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton2ActionPerformed(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Color Histogram Method", "CH with Perceptual Similarity", "Histogram Refinement with Color Coherence", "CH with Centering Refinement", "Custom Algorithm" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jComboBox1ActionPerformed(evt);
            }
        });

        resultsPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout resultsPanelLayout = new javax.swing.GroupLayout(resultsPanel);
        resultsPanel.setLayout(resultsPanelLayout);
        resultsPanelLayout.setHorizontalGroup(
            resultsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 677, Short.MAX_VALUE)
        );
        resultsPanelLayout.setVerticalGroup(
            resultsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 482, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(23, 23, 23)
                            .addComponent(jLabel1))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(resultsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(27, 27, 27))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(resultsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton1ActionPerformed
    {//GEN-HEADEREND:event_jButton1ActionPerformed
        // TODO add your handling code here:

        // TODO add your handling code here:
        JFileChooser fileChooser = new JFileChooser();
        if (IMAGE_DIRECTORY == null)
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        else
            fileChooser.setCurrentDirectory(new File(IMAGE_DIRECTORY));
        int returnValue = fileChooser.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION)
            {
            try
                {
                File selectedFile = fileChooser.getSelectedFile();
                String filePath = selectedFile.getAbsolutePath();
                IMAGE_DIRECTORY = filePath.substring(0, filePath.lastIndexOf(File.separator));
                filePath = filePath.replace('\\', '/');
                IMAGE_DIRECTORY = IMAGE_DIRECTORY.replace('\\', '/') + "/";
                String fileName = selectedFile.getName();
                System.out.println(fileName + " found at: " + filePath);
                String[] parts = fileName.split("\\.");

                //playerNameBox.setText(parts[0]);
                BufferedImage bufImg = ImageIO.read(selectedFile);
                jLabel2.setIcon(new ImageIcon(bufImg));
                queryImage.setColorArray(ColorProcessor.bufferedImageToColorArray(bufImg));
                queryImage.setHistogram(ColorProcessor.createHistogram(queryImage.getColorArray()));
                queryImage.setName(fileName);
                //queryImage.getHistogram().displayNormData();
                queryImage.setCenterCropHistogram(ColorProcessor.createCenterCropHistogram(queryImage.getColorArray()));
                queryImage.setOuterHistogram(ColorProcessor.createOuterHistogram(queryImage.getColorArray()));
                
                queryImage.colorCoherence.setCCRed(queryImage.getColorArray());
                queryImage.colorCoherence.setCCGreen(queryImage.getColorArray());
                queryImage.colorCoherence.setCCBlue(queryImage.getColorArray());
                
                
                
                queryImage.setLUVHistogram(ColorProcessor.convertToLUV(queryImage.getColorArray()));
                //queryImage.setLUVHistogram(ColorProcessor.createLUVHistogram(queryImage.getLUVArray()));
                
                queryImage.setChunkedHistogram(ColorProcessor.nineByNineChunking(queryImage.getColorArray()));
                
                queryImage.setContrastDeltaRed(ColorProcessor.nineByNineContrastDelta(queryImage.getColorArray(),"red"));
                queryImage.setContrastDeltaGreen(ColorProcessor.nineByNineContrastDelta(queryImage.getColorArray(),"green"));
                queryImage.setContrastDeltaBlue(ColorProcessor.nineByNineContrastDelta(queryImage.getColorArray(),"blue"));
                
                queryImage.setHorizontalContrastRed(ColorProcessor.nineByNineHorizontalContrast(queryImage.getColorArray(),"red"));
                queryImage.setHorizontalContrastGreen(ColorProcessor.nineByNineHorizontalContrast(queryImage.getColorArray(),"green"));
                queryImage.setHorizontalContrastBlue(ColorProcessor.nineByNineHorizontalContrast(queryImage.getColorArray(),"blue"));
                
                queryImage.setVerticalContrastRed(ColorProcessor.nineByNineVerticalContrast(queryImage.getColorArray(),"red"));
                queryImage.setVerticalContrastGreen(ColorProcessor.nineByNineVerticalContrast(queryImage.getColorArray(),"green"));
                queryImage.setVerticalContrastBlue(ColorProcessor.nineByNineVerticalContrast(queryImage.getColorArray(),"blue"));
                
                System.out.println("Query Image successfully loaded");

                // ColorProcessor.displayNormData(ColorProcessor.createHistogram(queryImageColorData));
                } catch (IOException ex)
                {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton2ActionPerformed
    {//GEN-HEADEREND:event_jButton2ActionPerformed
        // TODO add your handling code here:      
     
      //resultsPanel.repaint();
           resultsPanel.removeAll();
           resultsPanel.updateUI();
           candidates.clear();
      
      switch(jComboBox1.getSelectedIndex())
          {
          case 0:colorHistogramMethod();break;
          case 1:System.out.println("Perceptual Similarity"); perceptualSimilarity(); break;
          case 2:System.out.println("Color Coherence Activated");colorCoherence();break;
          case 3:colorHistogramWithCentering();System.out.println("Color histogram with centering refinement");break;
          case 4:System.out.println("Custom algorithm activated");customAlgorithm();break;
          default:System.out.println("Nothing has been set");break;
          }
        
    }//GEN-LAST:event_jButton2ActionPerformed

    public void colorHistogramMethod()
      {  
        int fileNum = 0;
        BufferedImage buffImg;
        File candidateFile;
        for (int i = 0; i < 2529; i++)
            {
            candidateFile = new File(IMAGE_DIRECTORY + fileNum + ".jpg");
            if (candidateFile.exists())
                {
                try
                    {
                    buffImg = ImageIO.read(candidateFile);
                    CandidateImage cand = new CandidateImage()
                            .setName(fileNum + "")
                            .setBuff(buffImg)
                            .setColorArray(ColorProcessor.bufferedImageToColorArray(buffImg));

                    cand
                            .setHistogram(ColorProcessor.createHistogram(cand.getColorArray()))
                            .setHistogramDifference(ColorProcessor.colorHistogramComparison(queryImage, cand));
                   //System.out.println("Total Histo: "+cand.getHistogram().normGreen[0]+" CenterCrop: "+ cand.getCenterCropHistogram().normGreen[0]+" Outer: "+ cand.getOuterHistogram().normGreen[0]);
                            candidates.add(cand);
                    
                    } catch (IOException ex)
                    {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            fileNum++;
            }
        candidates.sort((o1, o2) -> o1.getHistogramDifference().compareTo(o2.getHistogramDifference()));
        for (int i = 0; i < 84; i++)
            {
            System.out.println(candidates.get(i).getName() + ": " + candidates.get(i).getHistogramDifference());

           // JLabel resultIcon = new javax.swing.JLabel(new ImageIcon(candidates.get(i).getBuff()));
           
             Image img = candidates.get(i).getBuff();
            Image newimg = img.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
            JLabel resultIcon = new javax.swing.JLabel(new ImageIcon(newimg));
            resultIcon.setLocation(0 + 55 * (i % 12), 0 + 55 * (i / 12));
            resultIcon.setSize(50, 50);
            resultsPanel.add(resultIcon);
            }
        resultsPanel.repaint();
        System.out.println("IT OK");

      }
    
    public void perceptualSimilarity()
      {
          int fileNum = 0;
          BufferedImage buffImg;
          File candidateFile;
          
          for(int i = 0; i < 2529; i++){
              candidateFile = new File(IMAGE_DIRECTORY + fileNum + ".jpg");
              
              if(candidateFile.exists()){
                  try{
                      buffImg = ImageIO.read(candidateFile);
                      
                      CandidateImage cand = new CandidateImage()
                              .setName(fileNum + "")
                              .setBuff(buffImg)
                              .setColorArray(ColorProcessor.bufferedImageToColorArray(buffImg));
                      
                      cand.setLUVHistogram(ColorProcessor.convertToLUV(cand.getColorArray()));
                      cand.setColorSimilarity(ColorProcessor.simColLUV(queryImage, cand, simMatrix));
                      
                      candidates.add(cand);
                      
                  } catch(IOException ex){
                      Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                  }
              }
              
              fileNum++;
          }
          
          candidates.sort((o1, o2) -> o2.getColorSimilarity().compareTo(o1.getColorSimilarity()));
          
          for(int i = 0; i < 84; i++){
              System.out.println(candidates.get(i).getName() + ": " + candidates.get(i).getColorSimilarity());
              
              JLabel resultIcon = new javax.swing.JLabel(new ImageIcon(candidates.get(i).getBuff()));
              resultIcon.setLocation(0 + 55 * (i % 12), 0 + 55 * (i / 12));
              resultIcon.setSize(50, 50);
              resultsPanel.add(resultIcon);
          }
          resultsPanel.repaint();
          System.out.println("IT OK.");
      }
         public void colorCoherence()
      {  
        int fileNum = 0;
        BufferedImage buffImg;
        File candidateFile;
        for (int i = 0; i < 2529; i++)
            {
            candidateFile = new File(IMAGE_DIRECTORY + fileNum + ".jpg");
            if (candidateFile.exists())
                {
                try
                    {
                    buffImg = ImageIO.read(candidateFile);
                    CandidateImage cand = new CandidateImage()
                            .setName(fileNum + "")
                            .setBuff(buffImg)
                            .setColorArray(ColorProcessor.bufferedImageToColorArray(buffImg));

                    cand.colorCoherence.setCCRed(cand.getColorArray());
                    cand.colorCoherence.setCCGreen(cand.getColorArray());
                    cand.colorCoherence.setCCBlue(cand.getColorArray());
  
                    //System.out.println("query: "+ queryImage.getName()+" cand: "+cand.getName());
                    cand.setFusedScore(ColorProcessor.colorCoherenceComparison(queryImage, cand));
                    candidates.add(cand);
                    
                    } catch (IOException ex)
                    {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            fileNum++;
            }
        candidates.sort((o1, o2) -> o1.getFusedScore().compareTo(o2.getFusedScore()));

        for (int i = 0; i < 84; i++)
            {
            System.out.println(candidates.get(i).getName()+": " + candidates.get(i).getFusedScore());

           // JLabel resultIcon = new javax.swing.JLabel(new ImageIcon(candidates.get(i).getBuff()));
           
             Image img = candidates.get(i).getBuff();
            Image newimg = img.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
            JLabel resultIcon = new javax.swing.JLabel(new ImageIcon(newimg));
            resultIcon.setLocation(0 + 55 * (i % 12), 0 + 55 * (i / 12));
            resultIcon.setSize(50, 50);
            resultsPanel.add(resultIcon);
            }
        resultsPanel.repaint();
        System.out.println("IT OK");

      }
    
        public void customAlgorithm()
      {
        // 8x8 grid that is res invariant
        //get rough shape based on similarities in areas. 
        //Shape dominance
        
        int fileNum = 0;
        BufferedImage buffImg;
        File candidateFile;

        for (int i = 0; i < 2529; i++)
            {
            candidateFile = new File(IMAGE_DIRECTORY + fileNum + ".jpg");
            if (candidateFile.exists())
                {
                try
                    {
                    buffImg = ImageIO.read(candidateFile);
                    CandidateImage cand = new CandidateImage()
                            .setName(fileNum + "")
                            .setBuff(buffImg)
                            .setColorArray(ColorProcessor.bufferedImageToColorArray(buffImg));
                            
                    cand.setChunkedHistogram(ColorProcessor.nineByNineChunking(cand.getColorArray()));
                    cand.setCenterCropHistogram(ColorProcessor.createCenterCropHistogram(cand.getColorArray()));
                    cand.setHistogramDifference(ColorProcessor.chunkedHistogramComparison(queryImage, cand));
                    
                cand.setContrastDeltaRed(ColorProcessor.nineByNineContrastDelta(cand.getColorArray(), "red"));
                cand.setContrastDeltaGreen(ColorProcessor.nineByNineContrastDelta(cand.getColorArray(),"green"));
                cand.setContrastDeltaBlue(ColorProcessor.nineByNineContrastDelta(cand.getColorArray(),"blue"));
               
                    cand.setContrastDeltaScore(ColorProcessor.compareContrastDelta(queryImage, cand));
                    
                cand.setHorizontalContrastRed(ColorProcessor.nineByNineHorizontalContrast(cand.getColorArray(),"red"));
                cand.setHorizontalContrastGreen(ColorProcessor.nineByNineHorizontalContrast(cand.getColorArray(),"green"));
                cand.setHorizontalContrastBlue(ColorProcessor.nineByNineHorizontalContrast(cand.getColorArray(),"blue"));
                
                cand.setVerticalContrastRed(ColorProcessor.nineByNineVerticalContrast(cand.getColorArray(),"red"));
                cand.setVerticalContrastGreen(ColorProcessor.nineByNineVerticalContrast(cand.getColorArray(),"green"));
                cand.setVerticalContrastBlue(ColorProcessor.nineByNineVerticalContrast(cand.getColorArray(),"blue"));
                
                    
                    cand.setVerticalContrastScore(ColorProcessor.compareVerticalContrast(queryImage, cand));
                    cand.setHorizontalContrastScore(ColorProcessor.compareHorizontalContrast(queryImage, cand));
//                    cand.setHistogramDifference(ColorProcessor.centerCropColorHistogramComparison(queryImage, cand));
//                    cand.setHistogramDifference(ColorProcessor.centerCropColorHistogramComparison(queryImage, cand));
//                    
                    //cand.setFusedScore(cand.getContrastDeltaScore()/8+cand.getHistogramDifference());
                    cand.setFusedScore((cand.getHorizontalContrastScore()+cand.getVerticalContrastScore())/64+cand.getContrastDeltaScore()/128+ cand.getHistogramDifference()*4);
                    candidates.add(cand);
                    
                    } catch (IOException ex)
                    {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            fileNum++;
            }
        candidates.sort((o1, o2) -> o1.getFusedScore().compareTo(o2.getFusedScore()));
    
        for (int i = 0; i < 84; i++)
            {
            System.out.println(candidates.get(i).getName() + ": " + candidates.get(i).getFusedScore());


            Image img = candidates.get(i).getBuff();
            Image newimg = img.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
            JLabel resultIcon = new javax.swing.JLabel(new ImageIcon(newimg));
            
   
            
            resultIcon.setLocation(0 + 55 * (i % 12), 0 + 55 * (i / 12));
            resultIcon.setSize(50, 50);
            resultsPanel.add(resultIcon);
            }
        resultsPanel.repaint();
        System.out.println("IT OK");

      }
    
    public void colorHistogramWithCentering()
      {
       
        int fileNum = 0;
        BufferedImage buffImg;
        File candidateFile;

        for (int i = 0; i < 2529; i++)
            {
            candidateFile = new File(IMAGE_DIRECTORY + fileNum + ".jpg");
            if (candidateFile.exists())
                {
                try
                    {
                    buffImg = ImageIO.read(candidateFile);
                    CandidateImage cand = new CandidateImage()
                            .setName(fileNum + "")
                            .setBuff(buffImg)
                            .setColorArray(ColorProcessor.bufferedImageToColorArray(buffImg));

                    cand
                            
                            .setCenterCropHistogram(ColorProcessor.createCenterCropHistogram(cand.getColorArray()))
                            .setOuterHistogram(ColorProcessor.createOuterHistogram(cand.getColorArray()))
                            .setHistogramDifference(ColorProcessor.centerCropColorHistogramComparison(queryImage, cand));
                    
                   //System.out.println("Total Histo: "+cand.getHistogram().normGreen[0]+" CenterCrop: "+ cand.getCenterCropHistogram().normGreen[0]+" Outer: "+ cand.getOuterHistogram().normGreen[0]);
                    candidates.add(cand);
                    
                    } catch (IOException ex)
                    {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            fileNum++;
            }
        candidates.sort((o1, o2) -> o1.getHistogramDifference().compareTo(o2.getHistogramDifference()));
        for (int i = 0; i < 84; i++)
            {
            System.out.println(candidates.get(i).getName() + ": " + candidates.get(i).getHistogramDifference());

            JLabel resultIcon = new javax.swing.JLabel(new ImageIcon(candidates.get(i).getBuff()));
            resultIcon.setLocation(0 + 55 * (i % 12), 0 + 55 * (i / 12));
            resultIcon.setSize(50, 50);
            resultsPanel.add(resultIcon);
            }
        resultsPanel.repaint();
        System.out.println("IT OK");

      }
    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jComboBox1ActionPerformed
    {//GEN-HEADEREND:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
      {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
            {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
                {
                if ("Nimbus".equals(info.getName()))
                    {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                    }
                }
            } catch (ClassNotFoundException ex)
            {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (InstantiationException ex)
            {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex)
            {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (javax.swing.UnsupportedLookAndFeelException ex)
            {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
            {
            public void run()
              {
                  
                GUI gui = new GUI();
                gui.setLocationRelativeTo(null);
                gui.setVisible(true);
                
              }
            });
      }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel resultsPanel;
    // End of variables declaration//GEN-END:variables
    }
