/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiremp1;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 *
 * @author Stanley
 */
public class CandidateImage
    {
     String name;
     BufferedImage buff;
     Color colorArray[][];
     Histogram histogram;
     Histogram centerCropHistogram;
     Histogram outerHistogram;
     Histogram[][] chunkedHistogram;
     ContrastValues contrastValues = new ContrastValues();
     public ColorCoherence colorCoherence = new ColorCoherence();
     Float contrastDeltaScore;
     Float verticalContrastScore;
     Float horizontalContrastScore;
     Float histogramDifference=0f;
     Float fusedScore;
     
     Double colorSimilarity;

    public Float getFusedScore()
      {
        return fusedScore;
      }

    public void setFusedScore(Float fusedScore)
      {
        this.fusedScore = fusedScore;
      }

    public Float getVerticalContrastScore()
      {
        return verticalContrastScore;
      }

    public void setVerticalContrastScore(Float verticalContrastScore)
      {
        this.verticalContrastScore = verticalContrastScore;
      }

    public Float getHorizontalContrastScore()
      {
        return horizontalContrastScore;
      }

    public void setHorizontalContrastScore(Float horizontalContrastScore)
      {
        this.horizontalContrastScore = horizontalContrastScore;
      }

    public CandidateImage(String name, BufferedImage buff, Color[][] colorArray, Histogram histogram, Float histogramDifference)
      {
        this.name = name;
        this.buff = buff;
        this.colorArray = colorArray;
        this.histogram = histogram;
        this.histogramDifference =histogramDifference;
      }

    public Histogram getCenterCropHistogram()
      {
        return centerCropHistogram;
      }

    public Float getContrastDeltaScore()
      {
        return contrastDeltaScore;
      }

    public void setContrastDeltaScore(Float contrastDeltaScore)
      {
        this.contrastDeltaScore = contrastDeltaScore;
      }

    public float[][] getVerticalContrastRed()
      {
        return contrastValues.verticalContrastRed;
      }
      public float[][] getVerticalContrastGreen()
      {
        return contrastValues.verticalContrastGreen;
      }
        public float[][] getVerticalContrastBlue()
      {
        return contrastValues.verticalContrastBlue;
      }

         public void setVerticalContrastRed(float[][] verticalContrast)
      {
        this.contrastValues.verticalContrastRed = verticalContrast;
      }
          public void setVerticalContrastGreen(float[][] verticalContrast)
      {
        this.contrastValues.verticalContrastGreen = verticalContrast;
      }
    public void setVerticalContrastBlue(float[][] verticalContrast)
      {
        this.contrastValues.verticalContrastBlue = verticalContrast;
      }

   public float[][] getHorizontalContrastRed()
      {
        return contrastValues.horizontalContrastRed;
      }
      public float[][] getHorizontalContrastGreen()
      {
        return contrastValues.horizontalContrastGreen;
      }
        public float[][] getHorizontalContrastBlue()
      {
        return contrastValues.horizontalContrastBlue;
      }

         public void setHorizontalContrastRed(float[][] horizontalContrast)
      {
        this.contrastValues.horizontalContrastRed = horizontalContrast;
      }
          public void setHorizontalContrastGreen(float[][] horizontalContrast)
      {
        this.contrastValues.horizontalContrastGreen = horizontalContrast;
      }
    public void setHorizontalContrastBlue(float[][] horizontalContrast)
      {
        this.contrastValues.horizontalContrastBlue = horizontalContrast;
      }


    public float[][] getContrastDeltaRed()
      {
        return this.contrastValues.contrastDeltaRed;
      }
    public float[][] getContrastDeltaGreen()
      {
        return this.contrastValues.contrastDeltaGreen;
      }
    public float[][] getContrastDeltaBlue()
      {
        return this.contrastValues.contrastDeltaBlue;
      }

    public void setContrastDeltaRed(float[][] contrastDelta)
      {
        this.contrastValues.contrastDeltaRed = contrastDelta;
      }

    public void setContrastDeltaGreen(float[][] contrastDelta)
      {
        this.contrastValues.contrastDeltaGreen = contrastDelta;
      }
    public void setContrastDeltaBlue(float[][] contrastDelta)
      {
        this.contrastValues.contrastDeltaBlue = contrastDelta;
      }
       
    
    public CIE_LUV[][] getLuvArray()
      {
        return luvArray;
      }

    public void setLuvArray(CIE_LUV[][] luvArray)
      {
        this.luvArray = luvArray;
      }

    public CandidateImage setCenterCropHistogram(Histogram centerCropHistogram)
      {
        this.centerCropHistogram = centerCropHistogram;
        return this;
      }

    public Histogram getOuterHistogram()
      {
        return outerHistogram;
      }

    public Histogram[][] getChunkedHistogram()
      {
        return chunkedHistogram;
      }

    public void setChunkedHistogram(Histogram[][] chunkedHistogram)
      {
        this.chunkedHistogram = chunkedHistogram;
      }

    public CandidateImage setOuterHistogram(Histogram outerHistogram)
      {
        this.outerHistogram = outerHistogram;
        return this;
      }
    

    
    public BufferedImage getBuff()
      {
        return buff;
      }

    public CandidateImage setBuff(BufferedImage buff)
      {
        this.buff = buff;
        return this;
      }

    public Float getHistogramDifference()
      {
        return histogramDifference;
      }

    public CandidateImage setHistogramDifference(float histogramDifference)
      {
        this.histogramDifference = histogramDifference;
        return this;
      }

    public CandidateImage()
      {
        
      }
  
    public String getName()
      {
        return name;
      }

    public CandidateImage setName(String name)
      {
        this.name = name;
        return this;
      }

    public Histogram getHistogram()
      {
        return histogram;
      }

    public CandidateImage setHistogram(Histogram histogram)
      {
        this.histogram = histogram;
        return this;
      }

    public Color[][] getColorArray()
      {
        return colorArray;
      }

    public CandidateImage setColorArray(Color[][] colorArray)
      {
        this.colorArray = colorArray;
        return this;
      }
 
    CIE_LUV luvArray[][];
     LUVHistogram luvHistogram;
    
    public CIE_LUV[][] getLUVArray(){
        return luvArray;
    }
    
    public CandidateImage setLUVArray(CIE_LUV[][] luvArray){
        this.luvArray = luvArray;
        return this;
    }
    
    public LUVHistogram getLUVHistogram(){
        return luvHistogram;
    }
    
    public CandidateImage setLUVHistogram(LUVHistogram luvHistogram){
        this.luvHistogram = luvHistogram;
        return this;
    }
    
    public Double getColorSimilarity(){
        return colorSimilarity;
    }
    
    public void setColorSimilarity(Double colorSimilarity){
        this.colorSimilarity = colorSimilarity;
    }
    
    }
