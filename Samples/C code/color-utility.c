//================================================================
//
//  Main Program for utility classes
//
//     Created       11/2/95
//     Last changed  11/2/95
//
//================================================================

#include "color-utility.h"


//**************************************************************
//
// class List
// maintains the single-linked list
//
//**************************************************************
//
// * Constructor for List
//
List:: List (const char *name1) {
  name = strdup(name1);
  next = NULL;
}

//
// * Destructor for List
// * Need to delete all items in the rest of list
//
List:: ~List() {
//  printf("  ~List: <%s> object to be deleted!\n", name);
  if (name != NULL) delete name;
  if (next != NULL) delete next;
}


//
// * Insert a new item at the end of the current list
//
void List:: Append (List *item) {
  if (next == NULL) next = item;   // no item in list yet
  else {
    List *nextList = next;
    while (nextList->GetNext() != NULL)  // find last item of list
      nextList = nextList->GetNext();
    nextList->SetNext(item);
  }
}


//
// * To return the named item. It returns the item if it is found,
// * otherwise it returns NULL
//
List* List:: Find (const char *name1) {
  List *itemFound = NULL;
  // check if the current node is the one desired
  if (strcmp(this->GetName(), name1) == 0) itemFound = this;
  else {
    List *nextItem = next;
    while (nextItem != NULL && itemFound == NULL) {
      if (strcmp(nextItem->GetName(), name1) == 0)
        itemFound = nextItem;
      else
	nextItem = nextItem->GetNext();
    }
  }
  return itemFound;
}


//
// * To unlink named item from list, but NOT deleting it.
// * It returns the item if it is found,
// * otherwise it returns NULL if an ERROR has occurred, such as item not found
//
List* List:: Detach (const char *name1) {
  List *itemDetached = NULL;
  if (next != NULL) {
    List *currList = next;
    List *nextList = next->GetNext();
    if (strcmp(next->GetName(), name1) == 0) {
      // to remove the first item on list
      itemDetached = next;
      next = nextList;
    }
    else {
      while (nextList != NULL) {
	if (strcmp(nextList->GetName(), name1) == 0) {
	  currList->SetNext(nextList->GetNext());
	  itemDetached = nextList;
          nextList = NULL;
	}
        else {
	  currList = nextList;
	  nextList = nextList->GetNext();
        }
      }
    }
  }
  if (itemDetached != NULL) itemDetached->SetNext (NULL);
  return itemDetached;
}

//
// * To print contents of List object
//
void List:: Print() {
  printf("\n**List::Print, Error in printing <%s>!!\n", name);
}

//
// * To print contents of whole List, starting from current node
//
void List:: PrintList() {
  Print();
  List* nextList = next;
  while (nextList != NULL) {
    nextList->Print();
    nextList = nextList->GetNext();
  }
}


//**************************************************************
//
// class Point <- List
// a 3D point of type List. It maintains coordinates (x,y,z)
//
//**************************************************************
//
// * Compute the square of distance from p2
//
DOUBLE Point:: SqDistanceFrom (Point *p2) {
  DOUBLE dx = x - p2->x;
  DOUBLE dy = y - p2->y;
  DOUBLE dz = z - p2->z;
  return (dx*dx + dy*dy + dz*dz);
}

//
// * Duplicate itself, without copying the next pointer
//
Point* Point:: Duplicate() {
  return (new Point (x, y, z, name));
}

//
// * convert Point to array of diemension 3
// * Input array must have at least 3 elements
//
void Point:: MakeArray (DOUBLE *array) {
  array[0] = x;
  array[1] = y;
  array[2] = z;
}
  
//
// * To print contents of Point object
//
void Point:: Print() {
  printf("   Point::Print, x=%2.1f, y=%2.1f, z=%2.1f, name=<%s>\n", x,y,z,name);
}

