//***********************************************************************
//
//    luv-color.h
//    ************
// Definition of classes for color computation
// 1) classes for color conversion
//
//     Created       11/2/95
//     Last changed   9/5/95
//
//***********************************************************************

#ifndef _luv_color_h_
#define _luv_color_h_

#include <stdio.h>                /* standard C library files   */
#include <stdlib.h>
#include <strings.h>
#include <math.h>

#include "color-utility.h"

//=======================================================================
//
// 1) The following classess defines the utilities for color conversion
//
//======================================================================

//**************************************************************
//
// class Color <- Point
// to define a color in 3D space
// It modifies only the Print function of class Point.
//
//**************************************************************

class Color : public Point
{
 public:
    Color (DOUBLE x1=0., DOUBLE y1=0., DOUBLE z1=0., const char *name1=""):
      Point (x1, y1, z1, name1) {};
    virtual ~Color() {};
    virtual void Print();
};


//***********************************************************************
//
// class XYZRGB
// It performs the conversion between RGB and XYZ color spaces.
// It defines rgb white as [1,1,1], pre-computes the corresponding
// white point for XYZ color spaces.
//
//***********************************************************************

class XYZRGB
{
 protected:
   Color *xyzWhite; // corresponding white in XYZ = [Xo, Yo, Zo]
   Color *black;    // black in corresponding color space
   Color *green;    // green in correspondiong color space     
   float matRGBtoXYZ[3][3];  // RGB to XYZ conversion matrix
   float matXYZtoRGB[3][3];  // XYZ to RGB conversion matrix

 public:
   XYZRGB();    // setup the matrices, and the values of white points
   virtual ~XYZRGB();
     // provide two prototypes for every color conversion function
   virtual DOUBLE DistanceFromBlack (Color *bColor); //distance from Black
   virtual DOUBLE DistanceFromGreen (Color *gColor); //distance from Green
   virtual void Print();
 protected:
   Color *RGBtoXYZ (DOUBLE r, DOUBLE g, DOUBLE b);
   Color *RGBtoXYZ (Color *rgb);
   Color *XYZtoRGB (DOUBLE x, DOUBLE y, DOUBLE z);
   Color *XYZtoRGB (Color *xyz);
};


//***********************************************************************
//
// class LuvRGB
// It performs the conversion between SMPTE RGB and L*u*v* color spaces.
// It uses an intermediate color space XYZ to accomplish the conversion.
// It defines rgb white as [1,1,1], pre-computes the corresponding
// white point for XYZ color space, and the uo' and vo' values.
//
//***********************************************************************

class LuvRGB : public XYZRGB
{
 protected:
   DOUBLE u0, v0;     // Luv values corresponding to [Xo, Yo, Zo] 

 public:
   LuvRGB();    // setup the matrices, and the values of white points
   virtual ~LuvRGB() {}
     // provide two prototypes for every color conversion function
   Color *RGBtoLUV (DOUBLE r, DOUBLE g, DOUBLE b);
   Color *RGBtoLUV (Color *rgb);  // return a point in Luv space
   Color *LUVtoRGB (DOUBLE L, DOUBLE u, DOUBLE v);
   Color *LUVtoRGB (Color *luv);  // return a point in rgb space
};


//***********************************************************************
//
// class LabRGB
// It performs the conversion between RGB and L*a*b* color spaces.
// It uses an intermediate color space XYZ to accomplish the conversion.
// It defines rgb white as [1,1,1], pre-computes the corresponding
// white point for XYZ color space.
//
//***********************************************************************

class LabRGB : public XYZRGB
{
 public:
   LabRGB ();  // setup the matrices, and the values of white points
   virtual ~LabRGB() {}
     // provide two prototypes for every color conversion function
   Color *RGBtoLAB (DOUBLE r, DOUBLE g, DOUBLE b);
   Color *RGBtoLAB (Color *rgb);  // return a point in Luv space
   Color *LABtoRGB (DOUBLE L, DOUBLE a, DOUBLE b);
   Color *LABtoRGB (Color *lab);  // return a point in rgb space
};


//**************************************************************
//
// class LuvIndexing
// It maintains the set of distinct Luv colors used for indexing images
// The set of colors are stored in luv array
//
//**************************************************************

class LuvIndexing
{
 protected:
    char *luvFileName;  // name of luv index file
    int nLuv;     // # of entries in Luv array
    Color **luv;  // array to store the actual luv values according to defined index
    int nLindex;  // # of entries in L-index

 public:
    LuvIndexing (const char* luvFName);
    virtual ~LuvIndexing ();
    int GetLuvSize() { return nLuv; }
    int GetSizeOfLindex() { return nLindex; }
    void DisplayLuvContents ();

 protected:
    int ReadIndexFile();  // reads in luv colors from file
};


//**************************************************************
//
// class LuvLindex <-- LuvIndexing
// It maintains the set of distinct Luv colors used for indexing images
// The set of colors are stored in luv array
// The additional index based on L values are stored in Lindex array
// The main purpose of this function is to return the index value of
// an input Luv color
//
//**************************************************************
//
// The following struct is used within class LuvLIndex to facilitate the
// finding of index value for a given luv 
//
typedef struct {
   float maxL;   // L value falls in this range if its value lies
                  // between maxL(i-1) & maxL(i)
   int minIndex, maxIndex;  // entries in luv array within L value range
} L_INDEX;

#define MAX_LUV_RANGE  1000000.

class LuvLindex: public LuvIndexing
{
 protected:
    L_INDEX **Lindex;        // array containing L-indices
    LuvRGB *colorConverter;  // object to convert RGB to Luv colors

 public:
    LuvLindex (char* luvFName);
    virtual ~LuvLindex ();
    virtual int IndexOf (Color *thisLuv);  // return index of thisLuv color
    virtual int IndexOf (float L, float u, float v);  // overloaded function of IndexOf()
      // following function returns luv index given (r,g,b) and its range
    virtual int IndexOfRGB (int r, int g, int b, int range);
    void DisplayLindexContents ();

 protected:
    void SetupLindexArray();       // modify Luv values and setup L indices
};


//**************************************************************
//
// class LuvColorMatching <-- LuvIndexing
// It sets up the D-matrix to model the relationships between related
// colors. Color similarities are defined using distance.
// In particular, pc is used to limit the percentage of distance to consider
// Finally, it provides function to compute similarity between two histogram
//
//**************************************************************
//
// The following struct is used within class LuvDistanceIndex to maintain
// D-matrix between related colors
//

typedef struct {
   float factor;  // influence value of color (curr,index) to curr color
   int   index;   // column # of color with influence value, factor, to current color
} MTX_NODE;

class LuvColorMatching: public LuvIndexing
{
 protected:
    char *mtxFileName;  // name of file containing details of D-Matrix
    MTX_NODE **sMtx;    // sparse matrix containing non-zero netries of D-Mtx

 public:
    LuvColorMatching (const char* luvFName, const char* mtxFName,
                      int toCreateFile=false, float percentDistance=0.0);
        // set toCreateFile to true if new d-mtx data file is to be created
        // *** NOTE this is done only once
        // set percentDistance to deterine the raduius of influence of D-matrix
    virtual ~LuvColorMatching ();
    virtual MTX_NODE* GetDMtxRow (int i) { return sMtx[i]; }  // get row i of D-Mtx
    virtual float HistogramSimilarity (float *hist1, float *hist2);
    virtual float GetSimFactor (int BaseColor, int TestColor);
    void DisplayDMtxContents ();

 protected:
    virtual void CreateDMtxFile (float pcDistance);  // do once only for whole application
    virtual int SetupDMtx ();  // set up sMtx structure for on-line access
    // compute influence value given the color distance and the radius of influence
    virtual float ComputeInfluenceFactor (float distance, float radInfluence);
};


#endif

      
