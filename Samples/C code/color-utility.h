//================================================================
//
//  include file for basic utility classes
//  such as list and point classes
//
//     Created       11/2/95
//     Last changed  11/2/95
//
//================================================================

#ifndef _basic_utility_h_
#define _basic_utility_h_

#include <strings.h>
#include <stdio.h>

#include "standard.h"

//**************************************************************
//
// class List
// maintains the single-linked list
//
//**************************************************************

class List
{
 protected:
    char *name;     // identity of this element
    List *next;     // the next pointer

 public:
    List (const char *name1);
    virtual ~List();    // delete all items in the rest of list
    virtual void SetNext (List *item) { next = item; }
    virtual void Append (List *item);
    virtual List *Find (const char *name1);   // to return the named item
    virtual List *Detach (const char *name1); // to unlink named item, NOT delete
    virtual void Print();     // print contents of List
    virtual void PrintList(); // print contents of whole list
    virtual const char *GetName() { return (const char*) name; }
    virtual List *GetNext() { return next; }
};


//**************************************************************
//
// class Point <- List
// a 3D point of type List. It maintains coordinates (x,y,z)
//
//**************************************************************

class Point : public List
{
 public:
    DOUBLE x, y, z;   // makes it public to facilitate access

 public:
    Point (DOUBLE x1=0., DOUBLE y1=0., DOUBLE z1=0., const char *name1=""):
      List(name1) { x=x1; y=y1; z=z1; }
    virtual ~Point() {};
    virtual void Print();
    Point *Duplicate();  // make a duplicate of itself, without copying next       
    DOUBLE SqDistanceFrom (Point *p2); // square of distance from p2
    virtual void MakeArray (DOUBLE *array);    // pack x,y,z in 3-element array
    // x,y,z are accessed directly from external functions
};


#endif
