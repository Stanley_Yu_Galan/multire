//***********************************************************************
//
// luv-color.c
// -----------
// Definition of class functions for color computation
// 1) classes for color conversion
//
//     Created       11/2/95
//     Last changed   9/5/95
//
//***********************************************************************

#include "luv-color.h"

int quality = 3;  // define image quality desired in JPEG iamges

//=======================================================================
//
// 1) The following classess defines the utilities for color conversion
//
//======================================================================

//**************************************************************
//
// class Color <- Point
// to define a color in 3D space
// It modifies only the Print function of class Point.
//
//**************************************************************
//
// * To print contents of Color object
//
void Color:: Print() {
  printf(" %s(%3.3f, %3.3f, %3.3f)", name,x,y,z);
}

//***********************************************************************
//
// class XYZRGB
// It performs the conversion between RGB and XYZ color spaces.
// It defines rgb white as [1,1,1], pre-computes the corresponding
// white point for XYZ color space.
//
//***********************************************************************
//
// * Constructor for class XYZRGB
//
XYZRGB:: XYZRGB() {
  // setup the matrx for RGB to XYZ conversion
  matRGBtoXYZ[0][0] = 0.607;
  matRGBtoXYZ[0][1] = 0.174;
  matRGBtoXYZ[0][2] = 0.200;
  matRGBtoXYZ[1][0] = 0.299;
  matRGBtoXYZ[1][1] = 0.587;
  matRGBtoXYZ[1][2] = 0.114;
  matRGBtoXYZ[2][0] = 0.000;
  matRGBtoXYZ[2][1] = 0.066;
  matRGBtoXYZ[2][2] = 1.116;

  // setup the matrix for XYZ to RGB conversion
  matXYZtoRGB[0][0] =  1.910;
  matXYZtoRGB[0][1] = -0.532;
  matXYZtoRGB[0][2] = -0.288;
  matXYZtoRGB[1][0] = -0.985;
  matXYZtoRGB[1][1] =  2.000;
  matXYZtoRGB[1][2] = -0.028;
  matXYZtoRGB[2][0] =  0.058;
  matXYZtoRGB[2][1] = -0.118;
  matXYZtoRGB[2][2] =  0.898;

  xyzWhite = RGBtoXYZ (1., 1., 1.);  // convert RGB white to XYZ
  black = green = NULL;              // set them to NULL
  
}
  
//
// * Destructor function for class XYZRGB
//
XYZRGB:: ~XYZRGB () {
  delete xyzWhite;
  if (black != NULL) delete black;
  if (green != NULL) delete green;
}


//
// * Prototype 1 for converting a RGB color to XYZ color - linear conversion
//
Color* XYZRGB:: RGBtoXYZ (DOUBLE r, DOUBLE g, DOUBLE b) {
  Color *xyz = new Color(0., 0., 0., "XYZ");
  xyz->x = matRGBtoXYZ[0][0]*r + matRGBtoXYZ[0][1]*g +
           matRGBtoXYZ[0][2]*b;
  xyz->y = matRGBtoXYZ[1][0]*r + matRGBtoXYZ[1][1]*g +
           matRGBtoXYZ[1][2]*b;
  xyz->z = matRGBtoXYZ[2][0]*r + matRGBtoXYZ[2][1]*g +
           matRGBtoXYZ[2][2]*b;
  return xyz;
}

//
// * Prototype 2 for converting a RGB color to XYZ color - linear conversion
//
Color* XYZRGB:: RGBtoXYZ (Color *rgb) {
  return ( RGBtoXYZ (rgb->x, rgb->y, rgb->z) );
}


//
// * Prototype 1 for converting a XYZ color to a RGB color
// * transformation is again linear
//
Color* XYZRGB:: XYZtoRGB (DOUBLE x, DOUBLE y, DOUBLE z) {
  Color *rgb = new Color(0., 0., 0., "RGB");
  rgb->x = matXYZtoRGB[0][0]*x + matXYZtoRGB[0][1]*y +
           matXYZtoRGB[0][2]*z;
  rgb->y = matXYZtoRGB[1][0]*x + matXYZtoRGB[1][1]*y +
           matXYZtoRGB[1][2]*z;
  rgb->z = matXYZtoRGB[2][0]*x + matXYZtoRGB[2][1]*y +
           matXYZtoRGB[2][2]*z;
  return rgb;
}

//
// * Prototype 2 for converting a XYZ color to a RGB color
// * transformation is again linear
//
Color* XYZRGB:: XYZtoRGB (Color *xyz) {
  return ( XYZtoRGB (xyz->x, xyz->y, xyz->z) );
}
  
//
// * return the distance of a Luv color to the Luv black color
//
DOUBLE XYZRGB:: DistanceFromBlack (Color *bColor) {
  return ( sqrt (bColor->SqDistanceFrom (black)) );
}

//
// * return the distance of a Luv color to the Luv green color
//
DOUBLE XYZRGB:: DistanceFromGreen (Color *gColor) {
  return ( sqrt (gColor->SqDistanceFrom (green)) );
}

//
// * Prints contents of LuvRGB object
// * in particular the xyzWhite, u0, v0, and the matrices
//
void XYZRGB:: Print () {
  printf ("\n\nXYZRGB:: Print()\nxyzWhite");
  xyzWhite->Print();
  if (black != NULL) black->Print();
  if (green != NULL) green->Print();
  int i,j;
  printf ("RGB-XYZ conversion matrix:\n");
  for (i=0; i<3; i++) {
     for (j=0; i<3; j++) printf ("  %f,", matRGBtoXYZ[i][j]);
     printf ("\n");
  }
  printf ("XYZ-RGB conversion matrix:\n");
  for (i=0; i<3; i++) {
     for (j=0; i<3; j++) printf ("  %f,", matXYZtoRGB[i][j]);
     printf ("\n");
  }
}

#define LUVRGB_YRATIO_BOUND 0.008856
#define LUVRGB_L_BOUND2  25.0*pow(100.0*LUVRGB_YRATIO_BOUND, 0.33333) - 16.0
#define LUVRGB_L_BOUND1  903.3 * LUVRGB_YRATIO_BOUND

//***********************************************************************
//
// class LuvRGB
// It performs the conversion between SMPTE RGB and L*u*v* color spaces.
// It uses an intermediate color space XYZ to accomplish the conversion.
// It defines rgb white as [1,1,1], pre-computes the corresponding
// white point for XYZ color space, and the uo' and vo' values.
//
//***********************************************************************
//
// * Constructor for class LuvRGB
//
LuvRGB:: LuvRGB() {
  DOUBLE deno = xyzWhite->x + 15.0*xyzWhite->y + 3.0*xyzWhite->z;
  u0 = 4*xyzWhite->x / deno;
  v0 = 9*xyzWhite->y / deno;
  black = RGBtoLUV (0., 0., 0.);  // store the Luv Black
  green = RGBtoLUV (0., 1., 0.);  // store the Luv Green
}

//
// * Prototype 1 for converting a RGB color to Luv color
//
Color* LuvRGB:: RGBtoLUV (DOUBLE r, DOUBLE g, DOUBLE b) {
  Color *luv = new Color(0., 0., 0., "LUV");
  if (r!=0. || g!=0.0 || b!=0.0) {
    // non-black color
    Color *xyz = RGBtoXYZ (r, g, b); //convert from rgb to xyz color first

    // compute u' and v'
    DOUBLE deno = xyz->x + 15.0*xyz->y + 3.0*xyz->z;
    DOUBLE up = 4.0*xyz->x / deno;
    DOUBLE vp = 9.0*xyz->y / deno;

    // compute L*, u* and v*
    DOUBLE yRatio = xyz->y / xyzWhite->y;  // Y/Yo
    DOUBLE L;
    if (yRatio >= LUVRGB_YRATIO_BOUND) 
      L = 116.0 * pow (yRatio, 0.33333333) - 16.0;
    else
      L = 903.3 * yRatio;
    luv->x = L;                        // x ==> L*
    luv->y  = 13.0 * L * (up - u0);    // y ==> u*
    luv->z  = 13.0 * L * (vp - v0);    // z ==> v*
    delete xyz;
  }
  return luv;
}

//
// * Prototype 2 for converting a RGB color to Luv color
//
Color* LuvRGB:: RGBtoLUV (Color *rgb) {
  return ( RGBtoLUV (rgb->x, rgb->y, rgb->z) );
}


//
// * Prototype 1 for converting a Luv color to RGB color
//
Color* LuvRGB:: LUVtoRGB (DOUBLE L, DOUBLE u, DOUBLE v) {

  DOUBLE x, y, z;
  // First, convert from Luv to XYZ color
  if (abs(L)<0.5e-3 && abs(u)<0.5e-3 && abs(v)<0.5e-3) {
    x = y = z = 0.0;
  }
  else if ( abs(L) < 0.5e-3 || (L>=LUVRGB_L_BOUND1 && L<LUVRGB_L_BOUND2) ) {
    // Y almost zero, (u,v) non-zero OR
    // L in undefined range, point undefined
    return NULL;
  }
  else {
    // L in legitimate range, compute x, y, z
    if (L < LUVRGB_L_BOUND1) 
      y = xyzWhite->y * L / 903.3;
    else
      y = 0.01*xyzWhite->y * pow( ((L + 16.0)/25.0), 3.0);
    DOUBLE up = u / (13.0*L) + u0;   // u'
    DOUBLE vp = v / (13.0*L) + v0;   // v'
    x = (9.0*up) / (4.0*vp) * y;                         // X
    z = y * (12.0 - 3.0*up - 20.0*vp) / (4.0*vp);        // Z
  }
  
  // Now, convert from xyz to rgb
  return ( XYZtoRGB (x, y, z) );
}

//
// * Prototype 2 for converting a Luv color to RGB color
//
Color* LuvRGB:: LUVtoRGB (Color *luv) {
  return ( LUVtoRGB (luv->x, luv->y, luv->z) );
}


//***********************************************************************
//
// class LabRGB
// It performs the conversion between RGB and L*a*b* color spaces.
// It uses an intermediate color space XYZ to accomplish the conversion.
// It defines rgb white as [1,1,1], pre-computes the corresponding
// white point for XYZ color space.
//
//***********************************************************************
//
// * Constructor for class LabRGB
//
LabRGB:: LabRGB() {
  black = RGBtoLAB (0., 0., 0.);  // store the Lab Black
  green = RGBtoLAB (0., 1., 0.);  // store the Lab Green
}

//
// * Prototype 1 for converting a RGB color to Lab color
//
Color* LabRGB:: RGBtoLAB (DOUBLE r, DOUBLE g, DOUBLE b) {
  Color *lab = new Color(0., 0., 0., "LAB");
  if (r!=0. || g!=0.0 || b!=0.0) {
    // non-black color
    Color *xyz = RGBtoXYZ (r, g, b); //convert from rgb to xyz color first

    // compute the ratios
    DOUBLE yRatio  = xyz->y / xyzWhite->y;
    DOUBLE yRatio3 = pow (yRatio, 0.33333);
    DOUBLE xRatio3 = pow (xyz->x / xyzWhite->x, 0.33333);
    DOUBLE zRatio3 = pow (xyz->z / xyzWhite->z, 0.33333);

    DOUBLE L;
    if (yRatio >= 0.008856) 
      L = 116.0 * yRatio3 - 16.0;
    else
      L = 903.3 * yRatio;
    lab->x = L;                             // x ==> L*
    lab->y  = 500.0 * (xRatio3 - yRatio3);  // y ==> a*
    lab->z  = 200.0 * (yRatio3 - zRatio3);  // z ==> b*
    delete xyz;
  }
  return lab;
}

//
// * Prototype 2 for converting a RGB color to Luv color
//
Color* LabRGB:: RGBtoLAB (Color *rgb) {
  return ( RGBtoLAB (rgb->x, rgb->y, rgb->z) );
}


//
// * Prototype 1 for converting a Lab color to RGB color
//
Color* LabRGB:: LABtoRGB (DOUBLE L, DOUBLE a, DOUBLE b) {

  // First, convert from Lab to XYZ color
  DOUBLE newL = (L + 16.) / 25.;
  DOUBLE c100 = pow (0.01, 0.33333);
  DOUBLE x = xyzWhite->x * pow ( (a/500. + c100*newL), 3.0 );
  DOUBLE y = xyzWhite->y * pow ( newL, 3.0);
  DOUBLE z = xyzWhite->z * pow ( (c100*newL - b/200.), 3.0);
  
  // Now, convert from xyz to rgb
  return ( XYZtoRGB (x, y, z) );
}

//
// * Prototype 2 for converting a Luv color to RGB color
//
Color* LabRGB:: LABtoRGB (Color *lab) {
  return ( LABtoRGB (lab->x, lab->y, lab->z) );
}



//**************************************************************
//
// class LuvIndexing
// It maintains the set of distinct Luv colors used for indexing images
// The set of colors are stored in luv array
//
//**************************************************************
//
// * The Constructor for LuvIndexing
//
LuvIndexing:: LuvIndexing (const char* luvFName)
{
  luvFileName = strdup (luvFName);
  nLuv = nLindex = 0;
  // read in and allocate storage spaces for luv array
  if (ReadIndexFile() == -1) exit (-1);

};


//
// * The Destructor for LuvIndexing
//
LuvIndexing:: ~LuvIndexing ()
{
  delete luvFileName;
  // de-allocate spaces for luv array setup in ReadIndexFile()
  if (luv != NULL) {
     for (int i=0; i<nLuv; i++) delete luv[i];
     delete luv;
  }
};


//
// * Read in and setup luv colors in luv array
// * this fucntion also allocates storage for luv and Lindex arrays
//
int LuvIndexing:: ReadIndexFile()
{
  FILE *indexFile;
  int i;

  indexFile = fopen (luvFileName, "r");
  if (indexFile == NULL) {
     printf ("Error in opening luv file <%s> in class LuvIndexing\n", luvFileName);
     return (-1);
  }

  if (feof(indexFile)) return (-1);

  // read in sizes of luv and Lindex arrays and allocate storages
  fscanf (indexFile, "%d %d", &nLuv, &nLindex);
  if (nLuv <= 0 || nLindex <= 0) {
     printf ("Error in class LuvIndexing\nnLuv = %d, nLindex = %d\n", nLuv, nLindex);
     return (-1);
  }

  luv = new Color* [nLuv];
  for (i=0; i<nLuv; i++) luv[i] = new Color;

  // Now read Luv values into luv arrays
  float L, u, v; 
  int index;
  for (i=0; i<nLuv; i++) {
     fscanf (indexFile, "%f %f %f %d", &L, &u, &v, &index);
     luv[i]->x = L;
     luv[i]->y = u;
     luv[i]->z = v;
  }

  fclose (indexFile);
  return (0);
} 
    

//
// * Display the contents of luv array
//
void LuvIndexing:: DisplayLuvContents ()
{
  printf ("\n\nLuvIndexing:: DisplayContents\n");
  printf ("nLuv = %d\n #   (L,u,v)\n", nLuv);
  for (int i=0; i<nLuv; i++)
     printf (" %d   (%f, %f, %f)\n", i, luv[i]->x, luv[i]->y, luv[i]->z);
}


//**************************************************************
//
// class LuvLindex <-- LuvIndexing
// It maintains the set of distinct Luv colors used for indexing images
// The set of colors are stored in luv array
// It maintains additional indices based on L values in Lindex array
// Main purpose of this function is to return the index value of an 
// input Luv color
//
//**************************************************************
//
// * The Constructor for LuvLindex
//
//LuvLindex:: LuvLindex (const char* luvFName): LuvIndexing (luvFName)
LuvLindex:: LuvLindex (char* luvFName): LuvIndexing (luvFName)
{
  colorConverter = new LuvRGB;
  // allocate storage and setup Lindex array -- nLindex is read in Class LuvIndexing
  SetupLindexArray();
};


//
// * The Destructor for LuvLindex
//
LuvLindex:: ~LuvLindex ()
{
  // de-allocate spaces for Lindex array setup in SetupLindexArray())
  for (int i=0; i<nLindex; i++)  delete Lindex[i];
  delete Lindex;
  delete colorConverter;
};


//
// * return the color index of an luv color
//
int LuvLindex:: IndexOf (Color *thisLuv)
{
  return (IndexOf (thisLuv->x, thisLuv->y, thisLuv->z) );
}


//
// * return the color index of (L,u,v) - Overloaded fuinction of above
//
int LuvLindex:: IndexOf (float L, float u, float v)
{
  int nL = 0;   // holds the curent Lindex
  while (nL < nLindex && L > Lindex[nL]->maxL) nL++;

  // Lindex[nL]->minIndex & maxIndex hold the range containing the reqd index
  for (int i=Lindex[nL]->minIndex; i<Lindex[nL]->maxIndex; i++) {
     if (u <= luv[i]->y) {       // within u value range
        if (v <= luv[i]->z)      // and within v value range
           return (i);
     }
  }
  return (-1);  // return error - can't find index for thisLuv??
}


//
// * return luv index given (r,g,b) and its range, or the rgbIndex
// * need to normalze (r,g,b) values to between 0 and 1 using range
//
int LuvLindex:: IndexOfRGB (int r, int g, int b, int range)
{
  Color *aLuv = colorConverter->RGBtoLUV ( (float)r/range, (float)g/range,
                                           (float)b/range );
  int index = IndexOf (aLuv);
  delete aLuv;
  return (index);
}


//
// * This function updates luv array to store boundary values between adjacent cells
// * It also setups indices in Lindex array to facilitate the finding of color index
// * for a given luv color
//
void LuvLindex:: SetupLindexArray ()
{
  float avgL, avgU, avgV;
  int m;

  // allocate storage for Lindex - assume nLindex is read in ReadIndexFile
  if (nLindex <= 0) {
     printf("Error in Lindex size, n=%d\n", nLindex);
     exit (-1);
  }
  Lindex = new L_INDEX* [nLindex];
  for (int s=0; s<nLindex; s++) Lindex[s] = new L_INDEX;
  int index = 0;     // current position of Lindex array

  // find range of luv entries with same L value
  int i = 0;    // index for L
  float L1 = luv[i]->x;
  while (i < nLuv) {
     int i2 = i + 1;    // upper bound for i with same L values
     while (i2 < nLuv && luv[i2]->x == L1) i2++;
     if (i2 < nLuv) {
        avgL = 0.5 * (luv[i2]->x + L1);
        L1 = luv[i2]->x;
     }
     else avgL = MAX_LUV_RANGE;

     // assign avgL values into luv array
     for (m=i; m<i2; m++) luv[m]->x = avgL;

     // setup appropriate entries in Lindex array
     Lindex[index]->maxL = avgL;
     Lindex[index]->minIndex = i;
     Lindex[index]->maxIndex = i2;
     index++;

     //*** Now update u values in luv array between i and i2-1
     int j = i;  // index for u
     float u1 = luv[j]->y;
     while (j < i2) {
        int j2 = j + 1;   // upper bound for j with same u values
        while (j2 < i2 && luv[j2]->y == u1) j2++;
        if (j2 < i2) {
           avgU = 0.5 * (luv[j2]->y + u1);
           u1 = luv[j2]->y;
        }
        else avgU = MAX_LUV_RANGE;

        // assign avgL values into luv array
        for (m=j; m<j2; m++) luv[m]->y = avgU;

        //*** Now update v values in luv array between j and j2-1
        for (int k=j; k<j2; k++) {
           if (k+1 < j2) avgV = 0.5 * (luv[k]->z + luv[k+1]->z);
           else          avgV = MAX_LUV_RANGE;
           luv[k]->z = avgV;  // assign avgV values into luv array
        }      // end of while-loop for k

        j = j2;
     }         // end of while-loop for j 
  
     i = i2;
  }           // end of while-loop for i      
}


//
// * Display the contents of luv and Lindex arrays
//
void LuvLindex:: DisplayLindexContents ()
{
  DisplayLuvContents ();
  printf ("\n\nLuvLindex:: DisplayContents\n");
  printf ("Lindex = %d\n  #   maxL    minIndex maxIndex\n", nLindex);
  for (int i=0; i<nLindex; i++)
     printf (" %d   %f    (%d, %d)\n", i, Lindex[i]->maxL, Lindex[i]->minIndex,
                                                           Lindex[i]->maxIndex);
}


//**************************************************************
//
// class LuvColorMatching <-- LuvIndexing
// It sets up D-Matrix to model the relationships between related colors.
// Color similarities are defined using distance in Luv color space.
// In particular, pc is used to limit the percentage of distance to consider
// Finally, it provides function to compute similarity between two histogram
//
//**************************************************************
//
// * The Constructor for LuvColorMatching
//
LuvColorMatching:: LuvColorMatching (const char* luvFName, const char* mtxFName,
                                     int toCreateFile, float percentDistance):
                   LuvIndexing (luvFName)
{
  // set toCreateFile to TRUE if new d-mtx data file is to be created
  // *** NOTE this is done only once
  // set percentDistance to deterine the raduius of influence of D-matrix 
  mtxFileName = strdup (mtxFName);
  sMtx = NULL;
  if (toCreateFile) CreateDMtxFile (percentDistance);
  SetupDMtx();
};


//
// * The Destructor for LuvColorMatching
//
LuvColorMatching:: ~LuvColorMatching () 
{
  delete mtxFileName;
  if (sMtx != NULL) {
     for (int i=0; i<nLuv; i++) delete sMtx[i];
     delete sMtx;
  }
}


//
// * Compute influence factors for D-matrix - influence factor (i,j) defines
// * the influence of color j to current color i
// * The full distance matrix is first computed and store in mtx
// * Based on mtx; entries in D-mtx are then computed using an appropriate
// * function and stored in mtxFile.
// * *** Note that this function should be called only once at the beginning.
// * The pcDistance is used to limit the influence of other colors to 
// * current color.
// * If colorDistance > pc*MaxDistance from current color, 
// * then its influence is zero
//

void LuvColorMatching:: CreateDMtxFile (float pcDistance)  
{
  int i, j, k;
  float **mtx;  
  // allocate storage for the upper triangular matrix for distance & similarity  
  mtx = new float* [nLuv];
  for (i=0; i<nLuv; i++) mtx[i] = new float [nLuv]; 

  // compute the simiarity matrix between related colors
  float maxDist = 0.0;
  for (i=0; i<nLuv; i++) {
     mtx[i][i] = 1.0;
     Color *currLuv = luv[i];
     for (j=i+1; j<nLuv; j++) {
        float dist = sqrt ( currLuv->SqDistanceFrom (luv[j]) );
        mtx[i][j] = dist;
        maxDist = max (maxDist, dist);
     }
  }

/*
  // Display mtx matrix
  printf ("\n\nDistance Matrix:\nmaxDist = %f\n", maxDist);
  for (i=0; i<nLuv; i++) {
     printf ("row[%d]:",  i+1);
     for (j=i+1; j<nLuv; j++) {
       if (j/8*8 == j) printf ("\n");
       printf(" %f", mtx[i][j]);
     }
     printf ("\n");
  }
*/

  // compute distance of influence - beyond which colors have no influence
  // on current color - ie similarity value = 0
  float radInfluence = pcDistance * maxDist;
  for (i=0; i<nLuv; i++) 
  for (j=i+1; j<nLuv; j++) 
     mtx[i][j] = ComputeInfluenceFactor (mtx[i][j], radInfluence);
  
  // Write non-zeros of D-matrix to data file for future use

  FILE *mtxFile = fopen (mtxFileName, "w");
  for (i=0; i<nLuv; i++) {
     //*** count the no. of non-zeros entries in row i
     int count = 0;
     // count # of non-zeros before index i
     for (j=0; j<i; j++) if (mtx[j][i] != 0.0) count++;
     // count # of non-zeros after index i
     for (k=i+1; k<nLuv; k++) if (mtx[i][k] != 0.0) count++;

     //*** write information on row i to mtxFile   
     fprintf (mtxFile, "%d %d",  i, count);  // write row # and count
     for (j=0; j<i; j++)
       if (mtx[j][i] != 0.0) fprintf (mtxFile, " %d %f", j, mtx[j][i]);
     for (k=i+1; k<nLuv; k++) 
       if (mtx[i][k] != 0.0) fprintf (mtxFile, " %d %f", k, mtx[i][k]);
     fprintf (mtxFile, "\n");
  }
  fclose (mtxFile);

  // de-allocate storage for mtx
  for (i=0; i<nLuv; i++) delete mtx[i];
  delete mtx;
  // de-allocate storage for luv (setup in ReadIndexFile() )
  for (i=0; i<nLuv; i++) delete luv[i];
  delete luv;
  luv = NULL;
}


//
// * Read D-Mtx from data file and setup data-structure in core for
// * computing color similarities.
// * Color-Similarity = Hist1 * D-Mtx * Hist2
//
int LuvColorMatching:: SetupDMtx ()  
{
  FILE *mtxFile = fopen (mtxFileName, "r");
  if (mtxFile == NULL) {
     printf ("Error in opening mtxfile <%s>!\nIn class LuvDistanceMtx::SetupDMtx()n",
             mtxFileName);
     return (-1);
  }
  if (feof(mtxFile)) return (-1);

  // allocate storage for sMtx
  sMtx = new MTX_NODE* [nLuv];
  int row, column, n;
  float value;
  for (int i=0; i<nLuv; i++) {
     // read in D-Mtx row by row and store in matrix sMtx
     fscanf (mtxFile, "%d %d", &row, &n);  // n: # of non-zero entries in D-mtx
     if (row != i) {
        printf ("Error in reading mtxfile, row, i=%d %d!!\n", row, i);
        return (-1);
     }
     sMtx[i] = new MTX_NODE [n+1];
     // store # of non-zero entries in row i in first column of sMtx
     (sMtx[i][0]).index = n; 
     for (int j=1; j<=n; j++) {
        fscanf (mtxFile, "%d %f", &column, &value);
        (sMtx[i][j]).index = column;
        (sMtx[i][j]).factor = value;
     }
  }
  fclose (mtxFile);
  return (0);
}


//
// * compute influence value given the color distance and the raduis of influence
// * This function uses linear cap function to estimate influence factor
//
float LuvColorMatching:: ComputeInfluenceFactor (float distance, float radInfluence)
{
  float factor = 0.0;
  if (distance < radInfluence)            // within radius of influence
     factor = 1.0 - distance/radInfluence; // use linear cap function
  return factor;
}


//
// * compute histogram similarity between histogram vectors hist1 & hist2
// * It first compute the difference as
// *     Difference = (hist1-hist2) * M * (hist1-hist2)
// * Then compute the similarity as:
// *     Similarity = 1.0 - Difference
// * Please note that the similarity value returned is not normalized
//

// Weightage depends on queryHist
float LuvColorMatching:: HistogramSimilarity (float *hist, float *queryHist)
{
  int i;

  // for exact image, sim = 1
  float sim = 0;  

  float histSum = 0;
  float *histSim = new float [nLuv];
  float color_noise = 0.005; 	// 0.5%
  
  for (i=0; i<nLuv; i++)
  {
      if (queryHist[i] > color_noise)  {  
         histSim[i] = 1 - abs (queryHist[i] - hist[i])
			/max(queryHist[i], hist[i]);
      //printf ("Hist %d = %.6f\n", i, queryHist[i]); 
      }
      else histSim[i] = 0;
      histSum += queryHist[i];
  }

  if (histSum == 0) return 0;

  for (i=0; i<nLuv; i++) {

     int n = sMtx[i][0].index; // # of non-zero entries in row i
     float hSim = histSim[i];
     float color_sim = hSim;    // similarity contribution by the exact color   

     if (hSim > 0) {
        // similarity contriibution by the perceptually similar colors
        for (int j=1; j<=n; j++) {
           int k = sMtx[i][j].index;  // luv color of similar color

           color_sim += hSim * (1 - abs(queryHist[i] - hist[k])
		 	/max(queryHist[i], hist[k])) * sMtx[i][j].factor;
     	}
        if (color_sim > 1) color_sim = 1;  // to ensure sim <= 1

        // sim will never exceeds 1
        sim = sim + color_sim * queryHist[i];
     }     
  }

  if (histSum < 1) sim = sim / histSum; // to normalise sim

  return sim;   // return the similarity value  
}


//
// * Get the similarity factor of color TestColor wrt color BaseColor
//
float LuvColorMatching:: GetSimFactor(int BaseColor, int TestColor)
{
  if (BaseColor == TestColor)
    return 1.0;

  int n = sMtx[BaseColor][0].index;	// no of non-zero entries 
  
  float simfactor = 0.0;

  for (int j=1; j<=n; j++) {
    if (sMtx[BaseColor][j].index == TestColor) {
      simfactor = sMtx[BaseColor][j].factor;
      break;
    }  
  } 
  
  return simfactor;
  
}



//
// * Display the contents of D-Matrix
//
void LuvColorMatching:: DisplayDMtxContents ()
{
  printf ("\n\nColorDistanceMtx:: DisplayMtxContents nLuv=%d\n", nLuv);
  for (int i=0; i<nLuv; i++) {
     int n = sMtx[i][0].index;
     printf ("row[%d] = %d :",  i, n);
     for (int j=1; j<=n; j++) {
        printf(" (%d,%1.4f)", sMtx[i][j].index, sMtx[i][j].factor);
        if (j/5*5 == j) printf ("\n           ");
     }
     printf ("\n");
  }
}
